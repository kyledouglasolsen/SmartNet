﻿using System;
using UnityEngine;

public static class VectorExtensions
{
    public static Vector3 Abs(this Vector3 vector3)
    {
        return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
    }

    public static Vector2 Round(this Vector2 vector2, int precision = 2)
    {
        vector2.x = (float)Math.Round(vector2.x, precision);
        vector2.y = (float)Math.Round(vector2.y, precision);
        return vector2;
    }

    public static Vector3 Round(this Vector3 vector3, int precision = 2)
    {
        vector3.x = (float)Math.Round(vector3.x, precision);
        vector3.y = (float)Math.Round(vector3.y, precision);
        vector3.z = (float)Math.Round(vector3.z, precision);
        return vector3;
    }

    public static Vector3 Flat(this Vector3 vector3)
    {
        vector3.y = 0f;
        return vector3;
    }
}