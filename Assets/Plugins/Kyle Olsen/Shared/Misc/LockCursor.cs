﻿using UnityEngine;

public class LockCursor : Singleton<LockCursor>
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Lock(false);
        }
        else if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1))
        {
            Lock(true);
        }
    }

    public void Lock(bool doLock)
    {
        Cursor.lockState = doLock ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !doLock;
    }

    protected override void OnAwake()
    {
    }
}