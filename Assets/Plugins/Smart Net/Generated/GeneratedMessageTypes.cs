// Generated code. Do not modify.

using System;
using System.Runtime.CompilerServices;
using SmartNet.Messages;
using System.Collections.Generic;

namespace SmartNet
{
	internal abstract class SmartNetInvalidBaseType : INetMessage
	{
		public void OnSerialize(Writer writer)
		{
		}
		public void OnDeserialize(Reader reader)
		{
		}
	}
	
	internal class SmartNetInvalidType_InputMessage : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_Correction : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_WorldUpdate : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_RemoveOwner : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_SetOwner : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_UpdateTime : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_IdentityMessage : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_OwnerSpawn : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_WorldDestroy : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_AddPlayer : SmartNetInvalidBaseType { }
	internal class SmartNetInvalidType_WorldSpawn : SmartNetInvalidBaseType { }

	public class GeneratedMessageTypes : MessageTypes
	{
		public static void Initialize()
		{
			if (Instance == null || Instance.GetType() != typeof(GeneratedMessageTypes))
			{
				SetMessageType(new GeneratedMessageTypes());
			}
		}

		public override uint MessageCount { get; } = 11;
		
		public override Type[] AllTypes { get; } =
		{
			Type.GetType("InputMessage, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_InputMessage),
			Type.GetType("StateReplay+Correction, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_Correction),
			Type.GetType("WorldUpdater+WorldUpdate, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldUpdate),
			Type.GetType("SmartNet.Messages.RemoveOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_RemoveOwner),
			Type.GetType("SmartNet.Messages.SetOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_SetOwner),
			Type.GetType("SmartNet.Messages.UpdateTime, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_UpdateTime),
			Type.GetType("SmartNet.Messages.IdentityMessage, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_IdentityMessage),
			Type.GetType("SmartNet.Messages.OwnerSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_OwnerSpawn),
			Type.GetType("SmartNet.Messages.WorldDestroy, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldDestroy),
			Type.GetType("SmartNet.Messages.AddPlayer, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_AddPlayer),
			Type.GetType("SmartNet.Messages.WorldSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldSpawn),
		};

		private static readonly Dictionary<uint, Type> MessageIdToType = new Dictionary<uint, Type>()
		{
			{ 35, Type.GetType("InputMessage, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_InputMessage) },
			{ 36, Type.GetType("StateReplay+Correction, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_Correction) },
			{ 33, Type.GetType("WorldUpdater+WorldUpdate, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldUpdate) },
			{ 4, Type.GetType("SmartNet.Messages.RemoveOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_RemoveOwner) },
			{ 5, Type.GetType("SmartNet.Messages.SetOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_SetOwner) },
			{ 6, Type.GetType("SmartNet.Messages.UpdateTime, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_UpdateTime) },
			{ 2, Type.GetType("SmartNet.Messages.IdentityMessage, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_IdentityMessage) },
			{ 3, Type.GetType("SmartNet.Messages.OwnerSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_OwnerSpawn) },
			{ 7, Type.GetType("SmartNet.Messages.WorldDestroy, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldDestroy) },
			{ 1, Type.GetType("SmartNet.Messages.AddPlayer, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_AddPlayer) },
			{ 8, Type.GetType("SmartNet.Messages.WorldSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldSpawn) },
		};

		private static readonly Dictionary<Type, uint> TypeToMessageId = new Dictionary<Type, uint>()
		{
			{ Type.GetType("InputMessage, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_InputMessage), 35 },
			{ Type.GetType("StateReplay+Correction, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_Correction), 36 },
			{ Type.GetType("WorldUpdater+WorldUpdate, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldUpdate), 33 },
			{ Type.GetType("SmartNet.Messages.RemoveOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_RemoveOwner), 4 },
			{ Type.GetType("SmartNet.Messages.SetOwner, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_SetOwner), 5 },
			{ Type.GetType("SmartNet.Messages.UpdateTime, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_UpdateTime), 6 },
			{ Type.GetType("SmartNet.Messages.IdentityMessage, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_IdentityMessage), 2 },
			{ Type.GetType("SmartNet.Messages.OwnerSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_OwnerSpawn), 3 },
			{ Type.GetType("SmartNet.Messages.WorldDestroy, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldDestroy), 7 },
			{ Type.GetType("SmartNet.Messages.AddPlayer, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_AddPlayer), 1 },
			{ Type.GetType("SmartNet.Messages.WorldSpawn, SmartNet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null") ?? typeof(SmartNetInvalidType_WorldSpawn), 8 },
		};
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public override Type GetTypeFromId(uint typeId)
		{
			Type type;
			
			if (MessageIdToType.TryGetValue(typeId, out type))
			{
				return type;
			}
			
			return null;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public override uint GetTypeIdFromType(Type type)
		{
			uint typeId;
			
			if (TypeToMessageId.TryGetValue(type, out typeId))
			{
				return typeId;
			}
			
			return uint.MinValue;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public override uint GetTypeIdGeneric<T>()
		{
			return GetTypeId(typeof(T));
		}
	}
}
