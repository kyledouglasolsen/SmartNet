﻿using UnityEngine;

public class DecoupledRigibody : MonoBehaviour
{
    [SerializeField] private float mass = 1f;
    [SerializeField] private float drag = 0.05f;
    [SerializeField] private float angularDrag = 0f;
    [SerializeField] private int layer = Layers.Default;
    [SerializeField] private RigidbodyConstraints constraints = RigidbodyConstraints.None;
    [SerializeField] private CollisionDetectionMode collisionDetectionMode = CollisionDetectionMode.Discrete;

    public Rigidbody Rigidbody { get; private set; }

    public bool TryCreateRigidbody()
    {
        if (Rigidbody != null)
        {
            return false;
        }

        Rigidbody = new GameObject("Rigidbody").AddComponent<Rigidbody>();
        Rigidbody.gameObject.layer = layer;
        Rigidbody.constraints = constraints;
        Rigidbody.collisionDetectionMode = collisionDetectionMode;
        Rigidbody.mass = mass;
        Rigidbody.drag = drag;
        Rigidbody.angularDrag = angularDrag;

        Rigidbody.transform.localPosition = transform.localPosition;
        Rigidbody.transform.localRotation = transform.localRotation;

        return true;
    }

    private void Awake()
    {
        TryCreateRigidbody();
    }

    private void OnEnable()
    {
        if (Rigidbody != null)
        {
            Rigidbody.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (Rigidbody != null)
        {
            Rigidbody.gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if (Rigidbody != null)
        {
            Destroy(Rigidbody.gameObject);
        }
    }
}