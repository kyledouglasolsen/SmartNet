﻿using UnityEngine;

public struct InputData
{
    public readonly int Time;
    public readonly Keys Keys;
    public readonly Vector2 Mouse;
    public readonly int Hash;

    public InputData(int time, Keys keys, Vector2 mouse, int hash)
    {
        Time = time;
        Keys = keys;
        Mouse = mouse;
        Hash = hash;
    }
}