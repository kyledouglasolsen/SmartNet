﻿using SmartNet;
using UnityEngine;

public delegate void InputEvent(int time, Keys keys, Vector2 mouse, int hash);

public class InputHandler : MonoBehaviour, INetInitialize
{
    [SerializeField] private bool simulateRandomInputs = false;
    [SerializeField] private bool clientSidePredict = true;
    [SerializeField] private int inputsPerSecond = 33;
    [SerializeField] private int inputsBufferMilliseconds = 0;

    private IAuthoritative[] authoritatives;
    private AuthoritativeHash authoritativeHash;
    private SmartNetIdentity identity;
    private FixedUpdater fixedUpdater, serverUpdater;
    private Keys frameKeys;
    private Vector2 frameMouse;
    private int nextSendTime, speedHackCount;
    private readonly Writer clientAuthorityWriter = new Writer();
    private readonly Reader clientAuthorityReader = new Reader();
    private readonly InputMessage input = new InputMessage();
    private const int SpeedHackMillisecondsTarget = -100;
    private const int SpeedHackFrameTarget = 50;

    public InputHandlerTarget Target { get; private set; }
    public float DeltaTime { get; private set; }

    public event InputEvent InputEvent = delegate { };

    public void OnPreStart()
    {
        authoritatives = GetComponents<IAuthoritative>();

        Target = this.GetOrAddComponent<InputHandlerTarget>();
        identity = GetComponent<SmartNetIdentity>();

        DeltaTime = 1f / inputsPerSecond;
        nextSendTime = NetworkTime.Milliseconds + inputsBufferMilliseconds;
    }

    public void OnStartServer()
    {
        serverUpdater = new FixedUpdater(NetworkTime.Milliseconds, (int)(DeltaTime * 1000), time => { Target.SimulateInput(time, Keys.None, Vector2.zero, DeltaTime); },
            lerp => { Target.Interpolate(lerp); });

        identity.RegisterHandler<InputMessage>(info =>
        {
            var count = info.Message.Inputs.Count;

            if (count > 0)
            {
                var lastInput = info.Message.Inputs[count - 1];

                if (CheckForSpeedHack(lastInput.InputData.Time))
                {
                    return;
                }
            }

            for (var i = 0; i < count; ++i)
            {
                var frameInput = info.Message.Inputs[i].InputData;
                Target.SimulateInput(frameInput.Time, frameInput.Keys, frameInput.Mouse, DeltaTime);

                var hash = frameInput.Hash;
                clientAuthorityReader.Replace(info.Message.Inputs[i].AuthorityBytes);

                if (!Target.VerifyClientAuthority(clientAuthorityReader))
                {
                    hash = HashCode.Invalid;
                }

                InputEvent(frameInput.Time, frameInput.Keys, frameInput.Mouse, hash);
            }

            info.Message.Inputs.Clear();
        });

        Updater.PreUpdateEvent -= OnServerPreUpdate;
        Updater.PreUpdateEvent += OnServerPreUpdate;
    }

    private bool CheckForSpeedHack(int time)
    {
        var timeDifference = NetworkTime.Milliseconds - time;

        if (timeDifference >= SpeedHackMillisecondsTarget)
        {
            return false;
        }

        if (++speedHackCount > SpeedHackFrameTarget)
        {
            identity.Owner.Disconnect();
            return true;
        }

        return false;
    }

    public void OnStopServer()
    {
        identity.UnregisterHandler<InputMessage>();
        Updater.PreUpdateEvent -= OnServerPreUpdate;
    }

    public void OnStartOwner()
    {
        authoritativeHash = new AuthoritativeHash(authoritatives);

        fixedUpdater = new FixedUpdater(NetworkTime.Milliseconds, (int)(DeltaTime * 1000), time =>
        {
            var hash = 0;
            clientAuthorityWriter.SeekZero();

            if (clientSidePredict)
            {
                Target.SimulateInput(time, frameKeys, frameMouse, DeltaTime);
                Target.ExtractClientAuthority(clientAuthorityWriter);
                hash = authoritativeHash.Calculate();
                InputEvent(time, frameKeys, frameMouse, hash);
            }

            input.Add(new InputData(time, frameKeys, frameMouse, hash), clientAuthorityWriter.ToArray());
        }, lerp => { Target.Interpolate(lerp); });

        Updater.PreUpdateEvent -= PreUpdate;
        Updater.PreUpdateEvent += PreUpdate;
    }

    public void OnStopOwner()
    {
        Updater.PreUpdateEvent -= PreUpdate;
    }

    public void OnStartClient()
    {
    }

    public void OnStopClient()
    {
    }

    public void OnServerOwnerRemoved(Connection owner)
    {
        serverUpdater.SetTime(NetworkTime.Milliseconds);
    }

    public void OnServerOwnerAdded(Connection owner)
    {
    }

    private void PreUpdate()
    {
        var time = NetworkTime.Milliseconds;

        if (simulateRandomInputs)
        {
            frameKeys |= KeyExtensions.GetRandomKey();
            frameMouse += KeyExtensions.GetRandomMouse();
        }

        frameKeys |= KeyExtensions.GetKeys();
        frameMouse += KeyExtensions.GetMouse();

        if (fixedUpdater.Update(time))
        {
            frameKeys = Keys.None;
            frameMouse = Vector2.zero;

            if (NetworkTime.Milliseconds >= nextSendTime)
            {
                identity.Send(input, 1, false);
                input.Inputs.Clear();
                nextSendTime = NetworkTime.Milliseconds + inputsBufferMilliseconds;
            }
        }
    }

    private void OnServerPreUpdate()
    {
        if (identity.Owner != null)
        {
            return;
        }

        serverUpdater.Update(NetworkTime.Milliseconds);
    }

    private void OnDestroy()
    {
        Updater.PreUpdateEvent -= PreUpdate;
    }
}