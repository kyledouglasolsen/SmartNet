﻿using SmartNet;
using UnityEngine;

public class InputHandlerTarget : MonoBehaviour
{
    private IAuthoritative[] components;

    public void SimulateInput(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        for (var i = 0; i < components.Length; ++i)
        {
            if (!components[i].Enabled)
            {
                continue;
            }

            components[i].OnSimulate(time, keys, mouse, deltaTime);
        }
    }

    public void ExtractClientAuthority(Writer writer)
    {
        for (var i = 0; i < components.Length; ++i)
        {
            components[i].ExtractClientAuthority(writer);
        }
    }

    public bool VerifyClientAuthority(Reader reader)
    {
        var verified = true;

        for (var i = 0; i < components.Length; ++i)
        {
            verified &= components[i].VerifyClientAuthority(reader);
        }

        return verified;
    }

    public void Interpolate(float lerp)
    {
        for (var i = 0; i < components.Length; ++i)
        {
            if (!components[i].Enabled)
            {
                continue;
            }

            components[i].OnInterpolate(lerp);
        }
    }

    private void Awake()
    {
        components = GetComponents<IAuthoritative>();
    }
}