﻿using System;
using SmartNet;
using System.Collections.Generic;
using UnityEngine;

public class InputMessage : INetMessage
{
    private bool firstInput = true;
    private InputState previousInput = new InputState(InputFlags.None, new InputData(0, Keys.None, Vector2.zero, HashCode.Invalid), null);
    public readonly List<InputState> Inputs = new List<InputState>();

    public void Add(InputData input, byte[] authorityBytes)
    {
        var flags = CalculateFlags(firstInput, previousInput, input, authorityBytes);
        var newInputState = new InputState(flags, input, authorityBytes);
        Inputs.Add(newInputState);
        previousInput = newInputState;
        firstInput = false;
    }

    public void OnSerialize(Writer writer)
    {
        writer.WriteCompressed(Inputs.Count);

        for (var i = 0; i < Inputs.Count; ++i)
        {
            var flags = Inputs[i].Flags;
            writer.Write((byte)flags);
            writer.WriteCompressed(Inputs[i].InputData.Time);

            if ((flags & InputFlags.NewInputKeyData) != 0)
            {
                writer.WriteCompressed((uint)Inputs[i].InputData.Keys);
            }

            if ((flags & InputFlags.NewInputMouseData) != 0)
            {
                writer.Write(Inputs[i].InputData.Mouse);
            }

            if ((flags & InputFlags.NewHashData) != 0)
            {
                writer.WriteCompressed(Inputs[i].InputData.Hash);
            }

            if ((flags & InputFlags.AuthorityData) != 0)
            {
                writer.WriteBytesFull(Inputs[i].AuthorityBytes);
            }
        }
    }

    public void OnDeserialize(Reader reader)
    {
        var count = reader.ReadIntCompressed();

        for (var i = 0; i < count; ++i)
        {
            var flags = (InputFlags)reader.ReadByte();
            Keys keys;
            Vector2 mouse;
            int hash;

            var time = reader.ReadIntCompressed();

            if ((flags & InputFlags.NewInputKeyData) != 0)
            {
                keys = (Keys)reader.ReadUIntCompressed();
            }
            else
            {
                keys = previousInput.InputData.Keys;
            }

            if ((flags & InputFlags.NewInputMouseData) != 0)
            {
                mouse = reader.ReadVector2();
            }
            else
            {
                mouse = previousInput.InputData.Mouse;
            }

            if ((flags & InputFlags.NewHashData) != 0)
            {
                hash = reader.ReadIntCompressed();
            }
            else
            {
                hash = previousInput.InputData.Hash;
            }

            byte[] bytes = null;

            if ((flags & InputFlags.AuthorityData) != 0)
            {
                bytes = reader.ReadBytesAndSize();
            }

            Add(new InputData(time, keys, mouse, hash), bytes);
        }
    }

    private static InputFlags CalculateFlags(bool firstInput, InputState previousState, InputData newInputData, byte[] newAuthorityBytes)
    {
        var flags = InputFlags.None;

        if (newInputData.Keys != previousState.InputData.Keys)
        {
            flags |= InputFlags.NewInputKeyData;
        }

        if (newInputData.Mouse != previousState.InputData.Mouse)
        {
            flags |= InputFlags.NewInputMouseData;
        }

        if (newInputData.Hash != previousState.InputData.Hash)
        {
            flags |= InputFlags.NewHashData;
        }

        if (newAuthorityBytes != null && newAuthorityBytes.Length > 0)
        {
            flags |= InputFlags.AuthorityData;
        }

        return flags;
    }

    public struct InputState
    {
        public readonly InputFlags Flags;
        public readonly InputData InputData;
        public readonly byte[] AuthorityBytes;

        public InputState(InputFlags flags, InputData inputData, byte[] authorityBytes)
        {
            Flags = flags;
            InputData = inputData;
            AuthorityBytes = authorityBytes;
        }
    }

    [Flags]
    public enum InputFlags : byte
    {
        None = 0,
        NewInputKeyData = 1,
        NewInputMouseData = 2,
        NewHashData = 4,
        AuthorityData = 8
    }
}