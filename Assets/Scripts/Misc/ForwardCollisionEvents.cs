﻿using System;
using UnityEngine;

public class ForwardCollisionEvents : MonoBehaviour
{
    public event Action<Collision> CollisionEnterEvent = delegate { }, CollisionExitEvent = delegate { };

    private void OnCollisionEnter(Collision other)
    {
        CollisionEnterEvent(other);
    }

    private void OnCollisionExit(Collision other)
    {
        CollisionExitEvent(other);
    }
}