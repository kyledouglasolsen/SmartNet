﻿using UnityEngine;

public class Layers
{
    public const int Default = 0;
    public const int TransparentFX = 1;
    public const int IgnoreRaycast = 2;
    //public const int None = 3;
    public const int Water = 4;
    public const int UI = 5;
    //public const int None = 6;
    //public const int None = 7;
    public const int Vehicle = 8;
    public const int Player = 9;
    public const int WheelCollider = 10;

    public static readonly LayerMask UsableLayerMask = ~(1 << Vehicle | 1 << WheelCollider | 1 << IgnoreRaycast | 1 << UI);
    public static readonly LayerMask WalkableLayerMask = (1 << Default | 1 << Player);
}