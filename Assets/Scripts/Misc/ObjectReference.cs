﻿using UnityEngine;

public class ObjectReference : MonoBehaviour
{
    public Object Reference { get; private set; }

    public void SetReference(Object reference)
    {
        Reference = reference;
    }
}