﻿using SmartNet;
using UnityEngine;

public abstract class AuthoritativeCharacterController : AuthoritativeComponent<AuthoritativeCharacterController.MoverEvent>
{
    protected CharacterController CharacterController;

    public override bool SnapCorrections { get; } = false;

    protected virtual void Awake()
    {
        CharacterController = new GameObject("Character Controller").AddComponent<CharacterController>();

        CharacterController.height = 2f;
        CharacterController.center = new Vector3(0f, 1f, 0f);
        CharacterController.detectCollisions = false;
        CharacterController.enableOverlapRecovery = true;
        CharacterController.minMoveDistance = 0f;
        CharacterController.stepOffset = 0f;
        CharacterController.slopeLimit = 89f;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        TargetEvent = new MoverEvent(transform.localPosition, TargetEvent.Velocity, false);

        if (CharacterController != null)
        {
            CharacterController.enabled = true;
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        TargetEvent = new MoverEvent(transform.localPosition, Vector3.zero, false);
        
        if (CharacterController != null)
        {
            CharacterController.enabled = false;
        }
    }

    private void OnDestroy()
    {
        if (CharacterController != null)
        {
            Destroy(CharacterController.gameObject);
        }
    }

    protected override MoverEvent Simulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        CharacterController.transform.localPosition = TargetEvent.Position;

        bool jumping;
        var targetVelocity = CalculateVelocity(time, keys, mouse, deltaTime, out jumping).Round();

        var frameVelocity = targetVelocity * deltaTime;
        var horizontal = new Vector3(frameVelocity.x, 0f, frameVelocity.z);
        var vertical = new Vector3(0f, frameVelocity.y, 0f);

        CharacterController.Move(horizontal);
        CharacterController.Move(vertical);

        var isGrounded = !jumping && CharacterController.collisionFlags.HasFlag(CollisionFlags.Below);

        if (TargetEvent.IsGrounded != isGrounded)
        {
            OnGroundedChanged(isGrounded);
        }

        return new MoverEvent(CharacterController.transform.localPosition.Round(), targetVelocity, isGrounded);
    }

    public override void ApplyEvent(MoverEvent timelineEvent)
    {
        transform.localPosition = timelineEvent.Position;
    }

    protected override void SerializeAuthority(Writer writer)
    {
        writer.Write(TargetEvent.Position);
        writer.Write(TargetEvent.Velocity);
        writer.Write(TargetEvent.IsGrounded);
    }

    protected override MoverEvent DeserializeAuthority(Reader reader)
    {
        return new MoverEvent(reader.ReadVector3(), reader.ReadVector3(), reader.ReadBoolean());
    }

    public override int CalculateStateHash()
    {
        return HashCode.Start.Hash(TargetEvent.Position);
    }

    protected abstract Vector3 CalculateVelocity(int time, Keys keys, Vector2 mouse, float deltaTime, out bool jumping);
    protected abstract void OnGroundedChanged(bool isGrounded);

    public struct MoverEvent : ITimelineEvent<MoverEvent>
    {
        public readonly Vector3 Position;
        public readonly Vector3 Velocity;
        public readonly bool IsGrounded;

        public MoverEvent(Vector3 position, Vector3 velocity, bool isGrounded)
        {
            Position = position;
            Velocity = velocity;
            IsGrounded = isGrounded;
        }

        public MoverEvent InterpolateTo(MoverEvent otherEvent, float lerp)
        {
            return new MoverEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Vector3.Lerp(Velocity, otherEvent.Velocity, lerp), lerp < 1f ? IsGrounded : otherEvent.IsGrounded);
        }
    }
}