﻿using SmartNet;
using UnityEngine;

public abstract class AuthoritativeComponent<T> : MonoBehaviour, IAuthoritative, INetInitialize where T : ITimelineEvent<T>
{
    public T CurrentEvent { get; protected set; }
    public T InterpolatedEvent { get; protected set; }
    public T TargetEvent { get; protected set; }
    public T PreviousEvent { get; protected set; }

    public SmartNetIdentity Identity { get; private set; }

    public abstract bool SnapCorrections { get; }

    public bool Enabled => enabled;

    public void OnPreStart()
    {
        var rb = GetAuthoritativeRigidbody();

        if (rb != null)
        {
            SinglePhysics.Add(rb);
        }
    }

    public void OnStartServer()
    {
    }

    public void OnStopServer()
    {
    }

    public void OnStartOwner()
    {
    }

    public void OnStopOwner()
    {
    }

    public void OnStartClient()
    {
    }

    public void OnStopClient()
    {
    }

    public void OnServerOwnerRemoved(Connection owner)
    {
    }

    public void OnServerOwnerAdded(Connection owner)
    {
    }

    public void OnSimulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        PreviousEvent = TargetEvent;
        CurrentEvent = InterpolatedEvent;
        TargetEvent = Simulate(time, keys, mouse, deltaTime);

        if (Identity.IsServer)
        {
            InterpolatedEvent = TargetEvent;
            ApplyEvent(TargetEvent);
        }
    }

    public void OnInterpolate(float lerp)
    {
        InterpolatedEvent = CurrentEvent.InterpolateTo(TargetEvent, lerp);
        ApplyEvent(InterpolatedEvent);
    }

    public void OnSerializeAuthority(Writer writer)
    {
        SerializeAuthority(writer);
    }

    public void OnDeserializeAuthority(Reader reader)
    {
        TargetEvent = DeserializeAuthority(reader);

        if (SnapCorrections)
        {
            CurrentEvent = InterpolatedEvent = TargetEvent;
        }
    }

    public virtual void ExtractClientAuthority(Writer writer)
    {
    }

    public virtual bool VerifyClientAuthority(Reader reader)
    {
        return true;
    }

    public virtual Rigidbody GetAuthoritativeRigidbody()
    {
        return null;
    }

    protected virtual void OnEnable()
    {
        Identity = Identity ?? GetComponent<SmartNetIdentity>();
    }

    protected virtual void OnDisable()
    {
        var rb = GetAuthoritativeRigidbody();

        if (rb != null)
        {
            SinglePhysics.Remove(rb);
        }
    }

    public abstract void ApplyEvent(T timelineEvent);
    public abstract int CalculateStateHash();

    protected abstract T Simulate(int time, Keys keys, Vector2 mouse, float deltaTime);
    protected abstract void SerializeAuthority(Writer writer);
    protected abstract T DeserializeAuthority(Reader reader);
}