﻿public class AuthoritativeHash
{
    private readonly IAuthoritative[] components;

    public AuthoritativeHash(IAuthoritative[] components)
    {
        this.components = components;
    }

    public int Calculate()
    {
        var hash = HashCode.Start;

        for (var i = 0; i < components.Length; ++i)
        {
            hash = hash.Hash(components[i].CalculateStateHash());
        }

        return hash;
    }
}