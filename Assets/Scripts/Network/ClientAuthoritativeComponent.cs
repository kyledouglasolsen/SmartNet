﻿using SmartNet;

public abstract class ClientAuthoritativeComponent<T> : AuthoritativeComponent<T> where T : ITimelineEvent<T>
{
    public sealed override void ExtractClientAuthority(Writer writer)
    {
        OnExtractClientAuthority(writer);
    }

    protected abstract void OnExtractClientAuthority(Writer writer);

    public sealed override bool VerifyClientAuthority(Reader reader)
    {
        T newEvent;
        var valid = OnVerifyClientAuthority(reader, out newEvent);
        TargetEvent = newEvent;
        return valid;
    }

    protected abstract bool OnVerifyClientAuthority(Reader reader, out T targetEvent);

    public override int CalculateStateHash()
    {
        return 0;
    }
}