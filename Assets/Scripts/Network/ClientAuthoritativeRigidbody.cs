﻿using System;
using SmartNet;
using UnityEngine;

public abstract class ClientAuthoritativeRigidbody : ClientAuthoritativeComponent<ClientAuthoritativeRigidbody.RigidbodyEvent>
{
    private static readonly Collider[] OverlapCache = new Collider[50];

    [SerializeField] private LayerMask overlapMask = Physics.DefaultRaycastLayers;
    [SerializeField] private PhysicMaterial physicMaterial;

    private DecoupledRigibody decoupledRigidbody;
    private ForwardCollisionEvents forwardCollisionEvents;
    private BoxCollider boxCollider;

    protected Rigidbody Rigidbody => decoupledRigidbody?.Rigidbody;

    public override bool SnapCorrections { get; } = false;

    public override Rigidbody GetAuthoritativeRigidbody()
    {
        if (decoupledRigidbody == null)
        {
            decoupledRigidbody = this.GetOrAddComponent<DecoupledRigibody>();
            decoupledRigidbody.TryCreateRigidbody();
        }

        return decoupledRigidbody.Rigidbody;
    }

    protected override RigidbodyEvent Simulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        ApplyRigidbodySettings();
        ApplyRigidbodyForces(time, keys, mouse, deltaTime);

        SinglePhysics.Simulate(Rigidbody, deltaTime);

        return new RigidbodyEvent(Rigidbody);
    }

    protected abstract void ApplyRigidbodyForces(int time, Keys keys, Vector2 mouse, float deltaTime);

    public override void ApplyEvent(RigidbodyEvent timelineEvent)
    {
        transform.SetPositionAndRotation(timelineEvent.Position, timelineEvent.Rotation);
    }

    protected override void SerializeAuthority(Writer writer)
    {
        writer.Write(TargetEvent.Position);
        writer.Write(TargetEvent.Rotation);
        writer.Write(TargetEvent.Velocity);
        writer.Write(TargetEvent.AngularVelocity);
    }

    protected override RigidbodyEvent DeserializeAuthority(Reader reader)
    {
        return new RigidbodyEvent(reader.ReadVector3(), reader.ReadQuaternion(), reader.ReadVector3(), reader.ReadVector3());
    }

    protected override void OnExtractClientAuthority(Writer writer)
    {
        writer.Write(Rigidbody.position);
        writer.Write(Rigidbody.rotation);
    }

    protected override bool OnVerifyClientAuthority(Reader reader, out RigidbodyEvent targetEvent)
    {
        var clientPosition = reader.ReadVector3();
        var clientRotation = reader.ReadQuaternion();
        var distance = (TargetEvent.Position - clientPosition).magnitude;
        var valid = distance <= Mathf.Max(1f, TargetEvent.Velocity.magnitude);

        if (valid)
        {
            targetEvent = ComputeValidEvent(clientPosition, clientRotation);
        }
        else
        {
            targetEvent = new RigidbodyEvent(Rigidbody);
        }

        return valid;
    }

    protected virtual void Awake()
    {
        var rBody = GetAuthoritativeRigidbody();
        rBody.position = transform.position;
        rBody.rotation = transform.rotation;

        boxCollider = rBody.gameObject.AddComponent<BoxCollider>();
        boxCollider.center = new Vector3(0f, 0.75f, 0f);
        boxCollider.size = new Vector3(3f, 1.5f, 4.5f);
        boxCollider.material = physicMaterial;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        TargetEvent = new RigidbodyEvent(Rigidbody);
    }

    protected virtual RigidbodyEvent ComputeValidEvent(Vector3 fromPosition, Quaternion fromRotation)
    {
        var endPosition = fromPosition;
        var count = Physics.OverlapBoxNonAlloc(fromPosition, Vector3.Scale(boxCollider.bounds.extents, new Vector3(1.1f, 1f, 1.1f)), OverlapCache, fromRotation, overlapMask,
            QueryTriggerInteraction.Ignore);

        for (var i = 0; i < count; ++i)
        {
            if (boxCollider == OverlapCache[i])
            {
                continue;
            }

            Vector3 direction;
            float distance;
            var pos = OverlapCache[i].transform.position;
            var rot = OverlapCache[i].transform.rotation;

            if (Physics.ComputePenetration(boxCollider, fromPosition, fromRotation, OverlapCache[i], pos, rot, out direction, out distance))
            {
                endPosition += direction * distance;
            }

            var otherRigidbody = OverlapCache[i].attachedRigidbody;
            if (otherRigidbody != null)
            {
                OnCollidedWith(otherRigidbody);
            }
        }

        return new RigidbodyEvent(endPosition, fromRotation, TargetEvent.Velocity, TargetEvent.AngularVelocity);
    }

    private void OnCollidedWith(Rigidbody otherRigidbody)
    {
        if (otherRigidbody == null)
        {
            return;
        }

        var massDifference = Mathf.Clamp(Rigidbody.mass / otherRigidbody.mass, 0.1f, 2f);
        var velocityNormalized = Rigidbody.velocity.normalized;
        var directionToCenter = (otherRigidbody.worldCenterOfMass - Rigidbody.worldCenterOfMass);
        var averageImpactDirection = (velocityNormalized + directionToCenter) / 2f;
        var force = averageImpactDirection * massDifference;

        if (force.y < 0.5f)
        {
            force.y = 0.5f;
        }

        DebugExtensions.DebugArrow(otherRigidbody.position, force * 10f);

        otherRigidbody.AddForce(force, ForceMode.VelocityChange);

        var reference = otherRigidbody.GetComponentInParent<ObjectReference>()?.Reference as GameObject;

        if (reference == null)
        {
            return;
        }

        var mover = reference.GetComponent<RigidbodyMover>();

        if (mover == null)
        {
            return;
        }

        mover.AddForce(force);
    }

    private void ApplyRigidbodySettings()
    {
        Rigidbody.position = TargetEvent.Position;
        Rigidbody.rotation = TargetEvent.Rotation;
        Rigidbody.velocity = TargetEvent.Velocity;
        Rigidbody.angularVelocity = TargetEvent.AngularVelocity;
    }

    [Serializable]
    public struct RigidbodyEvent : ITimelineEvent<RigidbodyEvent>
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Velocity;
        public Vector3 AngularVelocity;

        public RigidbodyEvent(Rigidbody rigidbody) : this(rigidbody.position, rigidbody.rotation, rigidbody.velocity, rigidbody.angularVelocity)
        {
        }

        public RigidbodyEvent(Vector3 position, Quaternion rotation, Vector3 velocity, Vector3 angularVelocity)
        {
            Position = position;
            Rotation = rotation;
            Velocity = velocity;
            AngularVelocity = angularVelocity;
        }

        public RigidbodyEvent InterpolateTo(RigidbodyEvent otherEvent, float lerp)
        {
            return new RigidbodyEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Quaternion.Lerp(Rotation, otherEvent.Rotation, lerp), Vector3.Lerp(Velocity, otherEvent.Velocity, lerp),
                Vector3.Lerp(AngularVelocity, otherEvent.AngularVelocity, lerp));
        }
    }
}