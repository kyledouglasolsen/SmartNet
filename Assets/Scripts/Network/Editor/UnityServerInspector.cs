﻿using System.Reflection;
using SmartNet;
using UnityEditor;
using UnityEngine;

//[CustomEditor(typeof(UnityServer))]
public class UnityServerInspector : Editor
{
    private Server server;
    private FieldInfo knownIdentityInfo;
    private FieldInfo arrayField;

    private void OnEnable()
    {
        server = typeof(UnityServer).GetField("server", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(target) as Server;
        knownIdentityInfo = typeof(Server).GetField("knownIdentities", BindingFlags.Instance | BindingFlags.NonPublic);
        arrayField = typeof(KnownIdentities).GetField("array", BindingFlags.Instance | BindingFlags.NonPublic);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ++EditorGUI.indentLevel;

        if (server == null)
        {
            EditorGUILayout.LabelField("No server");
            return;
        }

        var knownIdentities = knownIdentityInfo.GetValue(server) as KnownIdentities;

        if (knownIdentities == null)
        {
            EditorGUILayout.LabelField("No known identities");
            return;
        }

        var array = arrayField.GetValue(knownIdentities) as SmartNetIdentity[];

        if (array == null)
        {
            EditorGUILayout.LabelField("Null array");
            return;
        }

        for (var i = 0; i < array.Length; ++i)
        {
            EditorGUILayout.ObjectField(new GUIContent(i.ToString()), array[i], typeof(KnownIdentities), true);
        }

        --EditorGUI.indentLevel;
    }
}