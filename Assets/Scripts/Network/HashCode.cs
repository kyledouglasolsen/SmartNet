﻿using System.Collections.Generic;

public struct HashCode
{
    private readonly int hashCode;

    public HashCode(int hashCode)
    {
        this.hashCode = hashCode;
    }

    public static HashCode Invalid { get; } = new HashCode(int.MinValue);
    public static HashCode Start { get; } = new HashCode(17);

    public static implicit operator int(HashCode hashCode) => hashCode.GetHashCode();

    public HashCode Hash<T>(T obj)
    {
        return unchecked(new HashCode(hashCode * 31 + EqualityComparer<T>.Default.GetHashCode(obj)));
    }

    public override int GetHashCode() => hashCode;
}