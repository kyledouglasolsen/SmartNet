﻿using SmartNet;
using UnityEngine;

public interface IAuthoritative
{
    bool Enabled { get; }

    void OnSimulate(int time, Keys keys, Vector2 mouse, float deltaTime);
    void OnInterpolate(float lerp);

    int CalculateStateHash();

    void ExtractClientAuthority(Writer writer);
    bool VerifyClientAuthority(Reader reader);

    void OnSerializeAuthority(Writer writer);
    void OnDeserializeAuthority(Reader reader);
}