﻿using SmartNet;

public interface IWorldComponent
{
    SmartNetIdentity Identity { get; }

    void Interpolate(int time);

    void OnSerialize(Writer writer);
    void OnDeserialize(int time, Reader reader);
}