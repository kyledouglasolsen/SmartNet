﻿using SmartNet;
using UnityEngine;

public class NetworkInstantiate : MonoBehaviour
{
    [SerializeField] private SmartNetIdentity prefab;

    public void Instantiate(KnownIdentities knownIdentities)
    {
        IdentityLibrary.Spawn(null, prefab, transform.position, transform.rotation, knownIdentities, NetworkId.Zero, true);
    }
}