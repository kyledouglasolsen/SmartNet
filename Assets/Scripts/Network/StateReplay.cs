﻿using System;
using SmartNet;
using UnityEngine;
using System.Collections.Generic;

public class StateReplay : MonoBehaviour, INetInitialize
{
    private const int InputHistoryStoreMilliseconds = 2000;
    private static Predicate<InputData> removeOldPredicate = input => NetworkTime.Milliseconds - input.Time > InputHistoryStoreMilliseconds;

    [SerializeField] private int minDelayBetweenCorrections = 500;
    private SmartNetIdentity identity;
    private InputHandler inputHandler;
    private AuthoritativeHash authoritativeHash;
    private Correction correction;
    private bool sendCorrection;
    private int nextCorrectTime;
    private readonly List<InputData> inputHistory = new List<InputData>();

    public void OnPreStart()
    {
        var authoritatives = GetComponents<IAuthoritative>();
        identity = GetComponent<SmartNetIdentity>();
        inputHandler = GetComponent<InputHandler>();
        correction = new Correction(authoritatives) {Time = NetworkTime.Milliseconds};
        authoritativeHash = new AuthoritativeHash(authoritatives);
    }

    public void OnStartServer()
    {
        inputHandler.InputEvent -= OnInputEvent;
        inputHandler.InputEvent += OnInputEvent;
        Updater.PostUpdateEvent -= OnServerPostUpdate;
        Updater.PostUpdateEvent += OnServerPostUpdate;
    }

    public void OnStopServer()
    {
        Updater.PostUpdateEvent -= OnServerPostUpdate;
        inputHandler.InputEvent -= OnInputEvent;
    }

    public void OnStartOwner()
    {
        identity.RegisterHandler(info =>
        {
            for (var i = 0; i < inputHistory.Count; ++i)
            {
                var input = inputHistory[i];

                if (input.Time <= correction.Time)
                {
                    continue;
                }

                inputHistory.RemoveRange(0, i);
                break;
            }

            Debug.LogError($"Replaying {inputHistory.Count} inputs...");

            for (var i = 0; i < inputHistory.Count; ++i)
            {
                var input = inputHistory[i];
                inputHandler.Target.SimulateInput(input.Time, input.Keys, input.Mouse, inputHandler.DeltaTime);
            }
        }, correction);

        inputHandler.InputEvent -= OnInputEvent;
        inputHandler.InputEvent += OnInputEvent;
    }

    public void OnStopOwner()
    {
        identity.UnregisterHandler<Correction>();
        inputHandler.InputEvent -= OnInputEvent;
    }

    public void OnStartClient()
    {
    }

    public void OnStopClient()
    {
    }

    public void OnServerOwnerRemoved(Connection owner)
    {
    }

    public void OnServerOwnerAdded(Connection owner)
    {
    }

    private void OnInputEvent(int time, Keys keys, Vector2 mouse, int hash)
    {
        if (identity.IsServer)
        {
            OnServerInput(time, hash);
        }

        if (identity.IsOwner)
        {
            OnOwnerInput(time, keys, mouse, hash);
        }
    }

    private void OnServerInput(int time, int hash)
    {
        if (sendCorrection || NetworkTime.Milliseconds < nextCorrectTime)
        {
            return;
        }

        var serverHash = authoritativeHash.Calculate();

        if (serverHash != hash)
        {
            correction.Capture(time);
            sendCorrection = true;
        }
    }

    private void OnOwnerInput(int time, Keys keys, Vector2 mouse, int hash)
    {
        inputHistory.Add(new InputData(time, keys, mouse, hash));
        inputHistory.RemoveAll(removeOldPredicate);
    }

    private void OnServerPostUpdate()
    {
        if (!sendCorrection)
        {
            return;
        }

        Debug.Log($"Sending correction for {gameObject.name} to {identity.Owner}...", gameObject);
        identity.Send(correction, 0, false);
        nextCorrectTime = NetworkTime.Milliseconds + minDelayBetweenCorrections;
        sendCorrection = false;
    }

    private class Correction : INetMessage
    {
        private readonly Writer internalWriter = new Writer();

        public int Time;
        public readonly IAuthoritative[] Components;

        public Correction()
        {
        }

        public Correction(IAuthoritative[] components)
        {
            Components = components;
        }

        public void Capture(int time)
        {
            Time = time;

            internalWriter.SeekZero();

            internalWriter.WriteCompressed(Time);

            for (var i = 0; i < Components.Length; ++i)
            {
                Components[i].OnSerializeAuthority(internalWriter);
            }
        }

        public void OnSerialize(Writer writer)
        {
            writer.Write(internalWriter);
        }

        public void OnDeserialize(Reader reader)
        {
            Time = reader.ReadIntCompressed();

            for (var i = 0; i < Components.Length; ++i)
            {
                Components[i].OnDeserializeAuthority(reader);
            }
        }
    }
}