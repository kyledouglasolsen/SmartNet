﻿using SmartNet;
using SmartNet.Messages;
using SmartNet.Profiler;
using UnityEngine;

public class UnityClient : MonoBehaviour
{
    [SerializeField] private string ip = "127.0.0.1";
    [SerializeField] private int port = 15337;
    [SerializeField] private ServerSettings settings;

    private void Start()
    {
        GeneratedMessageTypes.Initialize();

        NetworkClient.Configure(settings);

        NetworkClient.ConnectEvent += () =>
        {
            Debug.Log("Connected to server");
            NetworkClient.Send(new AddPlayer(), 0);
        };

        NetworkClient.DisconnectEvent += () => { Debug.Log("Disconnected from server"); };

        NetworkClient.Connect(ip, port);

        WorldUpdater.ConfigureClient(200);

        StatisticsUI.Show();
    }

    private void OnDestroy()
    {
        NetworkClient.Disconnect();
    }
}