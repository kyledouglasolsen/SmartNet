﻿using SmartNet;
using SmartNet.Profiler;
using UnityEngine;

public class UnityServer : MonoBehaviour
{
    [SerializeField] private string ip = "127.0.0.1";
    [SerializeField] private int port = 15337;
    [SerializeField] private ServerSettings settings;

    private void Start()
    {
        GeneratedMessageTypes.Initialize();

        NetworkServer.Configure(settings);

        NetworkServer.StartEvent += () =>
        {
            Debug.Log($"Start server id {NetworkServer.HostId}");

            foreach (var target in FindObjectsOfType<NetworkInstantiate>())
            {
                target.Instantiate(NetworkServer.KnownIdentities);
            }
        };

        NetworkServer.StopEvent += () => { Debug.Log($"Stop server id {NetworkServer.HostId}"); };
        NetworkServer.NewConnectionEvent += connection => { Debug.Log($"New connection from {connection.Ip}"); };
        NetworkServer.ConnectionDisconnectEvent += connection => { Debug.Log($"{connection.Ip} disconnected"); };

        NetworkServer.Start(ip, port);

        WorldUpdater.ConfigureServer(33, 200);

        StatisticsUI.Show();
    }

    private void OnDestroy()
    {
        NetworkServer.Stop();
    }
}