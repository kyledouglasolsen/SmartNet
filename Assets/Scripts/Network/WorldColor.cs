﻿using SmartNet;
using UnityEngine;

public class WorldColor : WorldComponent<WorldColor.ColorEvent>
{
    [SerializeField] private Color color = Color.white;
    private MeshRenderer meshRenderer;

    public override bool InterpolateOwner { get; } = true;

    public override ColorEvent GetEvent()
    {
        return new ColorEvent(color);
    }

    protected override void OnSerializeWorldComponent(Writer writer)
    {
        meshRenderer.material.color = color;
        writer.Write(color);
    }

    protected override ColorEvent OnDeserializeWorldComponent(Reader reader)
    {
        return new ColorEvent(reader.ReadColor());
    }

    protected override void OnApplyEvent(ColorEvent timelineEvent)
    {
        color = timelineEvent.Color;
        meshRenderer.material.color = color; 
    }

    private void Awake()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

    public struct ColorEvent : ITimelineEvent<ColorEvent>
    {
        public readonly Color Color;

        public ColorEvent(Color color)
        {
            Color = color;
        }

        public ColorEvent InterpolateTo(ColorEvent otherEvent, float lerp)
        {
            return new ColorEvent(Color.Lerp(Color, otherEvent.Color, lerp));
        }
    }
}