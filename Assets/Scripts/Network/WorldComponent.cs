﻿using SmartNet;
using UnityEngine;

public abstract class WorldComponent<T> : MonoBehaviour, IWorldComponent, INetInitialize where T : ITimelineEvent<T>, new()
{
    [HideInInspector] private SmartNetIdentity identity;
    protected HistoryEventTimeline<T> Timeline;
    protected HistoryEventInterpolate<T> TimelineInterpolate;

    public SmartNetIdentity Identity => identity;

    public abstract bool InterpolateOwner { get; }

    public abstract T GetEvent();

    public void AddEvent(int time, T timelineEvent)
    {
        Timeline.Add(time, timelineEvent);
        Timeline.Trim(time - WorldUpdater.HistorySaveMilliseconds);
    }

    public void OnSerialize(Writer writer)
    {
        OnSerializeWorldComponent(writer);
    }

    public void OnDeserialize(int time, Reader reader)
    {
        var newEvent = OnDeserializeWorldComponent(reader);
        Timeline.Add(time, newEvent);
    }

    protected abstract void OnSerializeWorldComponent(Writer writer);
    protected abstract T OnDeserializeWorldComponent(Reader reader);

    public void Interpolate(int time)
    {
        if (InterpolateOwner || !Identity.IsOwner)
        {
            TimelineInterpolate.ApplyState(time);
        }
    }

    protected virtual void OnEnable()
    {
        Timeline = new HistoryEventTimeline<T>();
        TimelineInterpolate = new HistoryEventInterpolate<T>(Timeline, OnApplyEvent);
        AddEvent(int.MinValue, GetEvent());
        identity = identity ?? GetComponent<SmartNetIdentity>();
    }

    protected abstract void OnApplyEvent(T timelineEvent);

    protected virtual void OnDisable()
    {
        WorldUpdater.Remove(this);
    }

    public void OnPreStart()
    {
        WorldUpdater.Add(this);
    }

    public void OnStartServer()
    {
    }

    public void OnStopServer()
    {
    }

    public void OnStartOwner()
    {
    }

    public void OnStopOwner()
    {
    }

    public void OnStartClient()
    {
    }

    public void OnStopClient()
    {
    }

    public void OnServerOwnerRemoved(Connection owner)
    {
    }

    public void OnServerOwnerAdded(Connection owner)
    {
    }
}