﻿using System;
using SmartNet;
using UnityEngine;

public class WorldTransform : WorldComponent<WorldTransform.WorldTransformEvent>
{
    private Vector3 previousPosition;
    private Quaternion previousRotation;
    private DecoupledRigibody decoupledRigidbody;

    public override bool InterpolateOwner { get; } = false;

    public override WorldTransformEvent GetEvent()
    {
        return new WorldTransformEvent(transform.localPosition, transform.localRotation);
    }

    protected override void OnSerializeWorldComponent(Writer writer)
    {
        var flags = CalculateFlags();

        writer.Write((byte)flags);

        if ((flags & TransformFlags.PositionChange) != 0)
        {
            writer.WriteCompressed(transform.localPosition);
            previousPosition = transform.localPosition;
        }

        if ((flags & TransformFlags.RotationChange) != 0)
        {
            writer.WriteCompressed(transform.localRotation);
            previousRotation = transform.localRotation;
        }
    }

    protected override WorldTransformEvent OnDeserializeWorldComponent(Reader reader)
    {
        var flags = (TransformFlags)reader.ReadByte();

        Vector3 position;
        Quaternion rotation;

        if ((flags & TransformFlags.PositionChange) != 0)
        {
            position = reader.ReadVector3Compressed();
        }
        else
        {
            position = previousPosition;
        }

        if ((flags & TransformFlags.RotationChange) != 0)
        {
            rotation = reader.ReadQuaternionCompressed();
        }
        else
        {
            rotation = previousRotation;
        }

        previousPosition = position;
        previousRotation = rotation;

        return new WorldTransformEvent(position, rotation);
    }

    protected override void OnApplyEvent(WorldTransformEvent timelineEvent)
    {
        transform.localPosition = timelineEvent.Position;
        transform.localRotation = timelineEvent.Rotation;

        if (decoupledRigidbody != null)
        {
            decoupledRigidbody.Rigidbody.transform.localPosition = timelineEvent.Position;
            decoupledRigidbody.Rigidbody.transform.localRotation = timelineEvent.Rotation;
        }
    }

    private void Awake()
    {
        decoupledRigidbody = GetComponent<DecoupledRigibody>();
        previousPosition = transform.localPosition;
        previousRotation = transform.localRotation;
    }

    private TransformFlags CalculateFlags()
    {
        var flags = TransformFlags.NoChange;

        if ((previousPosition - transform.localPosition).sqrMagnitude > 0.01f)
        {
            flags |= TransformFlags.PositionChange;
        }

        if (Quaternion.Angle(previousRotation, transform.localRotation) > 0.1f)
        {
            flags |= TransformFlags.RotationChange;
        }

        return flags;
    }

    public struct WorldTransformEvent : ITimelineEvent<WorldTransformEvent>
    {
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;

        public WorldTransformEvent(Vector3 position, Quaternion rotation)
        {
            Position = position;
            Rotation = rotation;
        }

        public WorldTransformEvent InterpolateTo(WorldTransformEvent otherEvent, float lerp)
        {
            return new WorldTransformEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Quaternion.Lerp(Rotation, otherEvent.Rotation, lerp));
        }
    }

    [Flags]
    public enum TransformFlags : byte
    {
        NoChange,
        PositionChange,
        RotationChange
    }
}