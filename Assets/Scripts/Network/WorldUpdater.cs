﻿using SmartNet;
using System.Collections.Generic;
using UnityEngine;

public static class WorldUpdater
{
    private static FixedUpdater fixedUpdater;
    private static float deltaTime;
    private static readonly WorldUpdate UpdateMessage = new WorldUpdate();
    private static readonly Dictionary<NetworkId, List<IWorldComponent>> ComponentLookup = new Dictionary<NetworkId, List<IWorldComponent>>();

    public static int InterpolateMilliseconds { get; private set; }
    public static int HistorySaveMilliseconds { get; private set; }

    public static void ConfigureServer(int updatesPerSecond, int interpolateMs)
    {
        InterpolateMilliseconds = interpolateMs;
        HistorySaveMilliseconds = interpolateMs * 5;
        deltaTime = 1f / updatesPerSecond;

        Application.targetFrameRate = updatesPerSecond;

        fixedUpdater = new FixedUpdater(NetworkTime.Milliseconds, (int)(deltaTime * 1000), time =>
        {
            UpdateMessage.Time = time;
            NetworkServer.SendToAll(UpdateMessage, 1);
        });

        Updater.PostUpdateEvent += () =>
        {
            var time = NetworkTime.Milliseconds;
            fixedUpdater.Update(time);
        };
    }

    public static void ConfigureClient(int interpolateMs)
    {
        InterpolateMilliseconds = interpolateMs;
        HistorySaveMilliseconds = interpolateMs * 5;

        NetworkClient.RegisterHandler<WorldUpdate>(info =>
        {
            // Only need handler register, all action happens in WorldUpdate OnDeserialize
        });

        Updater.PostUpdateEvent += () =>
        {
            var time = NetworkTime.Milliseconds;

            foreach (var kvp in ComponentLookup)
            {
                for (var i = 0; i < kvp.Value.Count; ++i)
                {
                    kvp.Value[i].Interpolate(time - InterpolateMilliseconds);
                }
            }
        };
    }

    public static void Add(IWorldComponent component)
    {
        List<IWorldComponent> components;

        var id = component.Identity.NetworkId;

        if (!ComponentLookup.TryGetValue(id, out components))
        {
            components = new List<IWorldComponent>();
            ComponentLookup[id] = components;
        }

        if (!components.Contains(component))
        {
            components.Add(component);
        }
    }

    public static void Remove(IWorldComponent component)
    {
        List<IWorldComponent> components;

        var id = component.Identity.NetworkId;

        if (ComponentLookup.TryGetValue(id, out components))
        {
            components.Remove(component);

            if (components.Count < 1)
            {
                ComponentLookup.Remove(id);
            }
        }
    }

    public static List<IWorldComponent> Get(NetworkId networkId)
    {
        List<IWorldComponent> components;
        return ComponentLookup.TryGetValue(networkId, out components) ? components : null;
    }

    private class WorldUpdate : INetMessage
    {
        public int Time;

        public void OnSerialize(Writer writer)
        {
            writer.WriteCompressed(Time);
            writer.WriteCompressed(ComponentLookup.Count);

            foreach (var kvp in ComponentLookup)
            {
                writer.Write(kvp.Key);

                for (var i = 0; i < kvp.Value.Count; ++i)
                {
                    kvp.Value[i].OnSerialize(writer);
                }
            }
        }

        public void OnDeserialize(Reader reader)
        {
            var time = reader.ReadIntCompressed();
            var count = reader.ReadIntCompressed();

            for (var i = 0; i < count; ++i)
            {
                var netId = reader.ReadNetworkId();
                var components = Get(netId);

                if (components == null)
                {
                    break;
                }

                for (var j = 0; j < components.Count; ++j)
                {
                    components[j].OnDeserialize(time, reader);
                }
            }
        }
    }
}