﻿using System;
using SmartNet;
using UnityEngine;

public class WorldVehicle : WorldComponent<WorldVehicle.WorldVehicleEvent>
{
    private Vehicle vehicle;
    private DecoupledRigibody decoupledRigidbody;

    private Vector3 previousPosition;
    private Quaternion previousRotation;
    private Vector3 previousVelocity;
    private float previousSteer;
    private float previousThrottle;

    public override bool InterpolateOwner { get; } = false;

    private Vector3 Velocity => decoupledRigidbody?.Rigidbody?.velocity ?? Vector3.zero;

    public override WorldVehicleEvent GetEvent()
    {
        return new WorldVehicleEvent(vehicle.transform.position, vehicle.transform.rotation, Velocity, vehicle.Steer, vehicle.Throttle);
    }

    protected override void OnSerializeWorldComponent(Writer writer)
    {
        var flags = CalculateFlags();

        writer.Write((byte)flags);

        if ((flags & VehicleFlags.PositionChange) != 0)
        {
            previousPosition = vehicle.transform.position;
            writer.WriteCompressed(previousPosition);
        }

        if ((flags & VehicleFlags.RotationChange) != 0)
        {
            previousRotation = vehicle.transform.rotation;
            writer.WriteCompressed(previousRotation);
        }

        if ((flags & VehicleFlags.VelocityChange) != 0)
        {
            previousVelocity = Velocity;
            writer.WriteCompressed(previousVelocity);
        }

        if ((flags & VehicleFlags.SteerChange) != 0)
        {
            previousSteer = vehicle.Steer;
            writer.WriteCompressed(previousSteer);
        }

        if ((flags & VehicleFlags.ThrottleChange) != 0)
        {
            previousThrottle = vehicle.Throttle;
            writer.WriteCompressed(vehicle.Throttle);
        }
    }

    protected override WorldVehicleEvent OnDeserializeWorldComponent(Reader reader)
    {
        var flags = (VehicleFlags)reader.ReadByte();

        Vector3 position;
        Quaternion rotation;
        Vector3 velocity;
        float steer;
        float throttle;

        if ((flags & VehicleFlags.PositionChange) != 0)
        {
            position = reader.ReadVector3Compressed();
            previousPosition = position;
        }
        else
        {
            position = previousPosition;
        }

        if ((flags & VehicleFlags.RotationChange) != 0)
        {
            rotation = reader.ReadQuaternionCompressed();
            previousRotation = rotation;
        }
        else
        {
            rotation = previousRotation;
        }

        if ((flags & VehicleFlags.VelocityChange) != 0)
        {
            velocity = reader.ReadVector3Compressed();
            previousVelocity = velocity;
        }
        else
        {
            velocity = previousVelocity;
        }

        if ((flags & VehicleFlags.SteerChange) != 0)
        {
            steer = reader.ReadFloatCompressed();
            previousSteer = steer;
        }
        else
        {
            steer = previousSteer;
        }

        if ((flags & VehicleFlags.ThrottleChange) != 0)
        {
            throttle = reader.ReadFloatCompressed();
            previousThrottle = throttle;
        }
        else
        {
            throttle = previousThrottle;
        }

        return new WorldVehicleEvent(position, rotation, velocity, steer, throttle);
    }

    protected override void OnApplyEvent(WorldVehicleEvent timelineEvent)
    {
        transform.SetPositionAndRotation(timelineEvent.Position, timelineEvent.Rotation);
        vehicle.ApplySteerAndThrottle(timelineEvent.Steer, timelineEvent.Throttle);

        if (decoupledRigidbody != null)
        {
            decoupledRigidbody.Rigidbody.position = timelineEvent.Position;
            decoupledRigidbody.Rigidbody.rotation = timelineEvent.Rotation;
            decoupledRigidbody.Rigidbody.velocity = timelineEvent.Velocity;

            // Can't do this until we've fixed issues with multiple object stepping losing velocity
            //SinglePhysics.Simulate(decoupledRigidbody.Rigidbody, Time.deltaTime);
        }
    }

    private void Awake()
    {
        decoupledRigidbody = GetComponent<DecoupledRigibody>();
        vehicle = GetComponent<Vehicle>();

        previousPosition = vehicle.transform.position;
        previousRotation = vehicle.transform.rotation;
        previousVelocity = Velocity;
        previousSteer = vehicle.Steer;
        previousThrottle = vehicle.Throttle;
    }

    private VehicleFlags CalculateFlags()
    {
        var flags = VehicleFlags.NoChange;

        if ((vehicle.transform.position - previousPosition).sqrMagnitude > 0.01f)
        {
            flags |= VehicleFlags.PositionChange;
        }

        if (Quaternion.Angle(vehicle.transform.rotation, previousRotation) > 0.1f)
        {
            flags |= VehicleFlags.RotationChange;
        }

        if ((Velocity - previousVelocity).sqrMagnitude > 0.01f)
        {
            flags |= VehicleFlags.VelocityChange;
        }

        if (Math.Abs(vehicle.Steer - previousSteer) > 0.01f)
        {
            flags |= VehicleFlags.SteerChange;
        }

        if (Math.Abs(vehicle.Throttle - previousThrottle) > 0.01f)
        {
            flags |= VehicleFlags.ThrottleChange;
        }

        return flags;
    }

    public struct WorldVehicleEvent : ITimelineEvent<WorldVehicleEvent>
    {
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;
        public readonly float Steer, Throttle;
        public readonly Vector3 Velocity;

        public WorldVehicleEvent(Vector3 position, Quaternion rotation, Vector3 velocity, float steer, float throttle)
        {
            Position = position;
            Rotation = rotation;
            Velocity = velocity;
            Steer = steer;
            Throttle = throttle;
        }

        public WorldVehicleEvent InterpolateTo(WorldVehicleEvent otherEvent, float lerp)
        {
            return new WorldVehicleEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Quaternion.Lerp(Rotation, otherEvent.Rotation, lerp), Vector3.Lerp(Velocity, otherEvent.Velocity, lerp),
                Mathf.Lerp(Steer, otherEvent.Steer, lerp), Mathf.Lerp(Throttle, otherEvent.Throttle, lerp));
        }
    }

    [Flags]
    public enum VehicleFlags : byte
    {
        NoChange = 0,
        PositionChange = 1,
        RotationChange = 2,
        VelocityChange = 4,
        SteerChange = 8,
        ThrottleChange = 16
    }
}