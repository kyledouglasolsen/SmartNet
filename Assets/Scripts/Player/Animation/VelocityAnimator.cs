﻿using SmartNet;
using UnityEngine;

public class VelocityAnimator : MonoBehaviour
{
    private static readonly int XId = Animator.StringToHash("X"), YId = Animator.StringToHash("Y");

    [SerializeField] private float delayBetweenVelocitySamples = 0.1f;
    [SerializeField] private float velocityMultiplier = 1f;
    private Animator animator;
    private Vector3 previousPosition;
    private Vector3 velocity;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        InvokeRepeating(nameof(OnUpdateVelocity), delayBetweenVelocitySamples, delayBetweenVelocitySamples);
        Updater.PostUpdateEvent += OnPostUpdate;
    }

    private void OnDisable()
    {
        CancelInvoke(nameof(OnUpdateVelocity));
        Updater.PostUpdateEvent -= OnPostUpdate;
    }

    private void OnUpdateVelocity()
    {
        var newPosition = transform.position;
        velocity = transform.InverseTransformDirection(newPosition - previousPosition);

        if (delayBetweenVelocitySamples <= 0f)
        {
            velocity /= Time.deltaTime;
        }
        else
        {
            velocity /= delayBetweenVelocitySamples;
        }

        velocity *= velocityMultiplier;

        previousPosition = newPosition;
    }

    private void OnPostUpdate()
    {
        animator.SetFloat(XId, velocity.x, 0.05f, Time.deltaTime);
        animator.SetFloat(YId, velocity.z, 0.05f, Time.deltaTime);
    }
}