﻿using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    private float followDistance = 8f;
    private Vector2 offset = new Vector2(1f, 1.5f);
    private Rotator rotator;

    public void GetTransformData(out Vector3 position, out Quaternion rotation)
    {
        var currentRotatorEvent = (Rotator.RotatorEvent)rotator.InterpolatedEvent;
        rotation = Quaternion.Euler(currentRotatorEvent.X, currentRotatorEvent.Y, 0f);
        var forward = rotation * Vector3.forward;
        var up = rotation * Vector3.up;
        var right = rotation * Vector3.right;

        var worldOffset = right * offset.x + up * offset.y;
        position = transform.position - forward * followDistance + worldOffset;
    }

    private void Start()
    {
        rotator = GetComponent<Rotator>();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Vector3 position;
        Quaternion rotation;

        GetTransformData(out position, out rotation);

        Gizmos.DrawWireSphere(position, 0.25f);
        DebugExtensions.DrawArrow(position, rotation * Vector3.forward, Color.red);
    }
}