﻿using UnityEngine;

public class CharacterControllerMover : AuthoritativeCharacterController, IPushable, IHaveVelocity
{
    [SerializeField] private float acceleration = 10f;
    [SerializeField] private float deceleration = 10f;
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float jumpHeight = 5f;

    private GroundDetector groundDetector;
    private Rotator rotator;

    public int Priority { get; } = 0;
    public Vector3 Position => TargetEvent.Position;
    public Vector3 Velocity => CharacterController.velocity;
    public bool CanBePushed => enabled;

    public void AddForce(Vector3 force)
    {
        TargetEvent = new MoverEvent(TargetEvent.Position, TargetEvent.Velocity + force, TargetEvent.IsGrounded);
    }

    public void SetGrounded(bool isGrounded)
    {
        TargetEvent = new MoverEvent(TargetEvent.Position, TargetEvent.Velocity, isGrounded);
    }

    protected override void Awake()
    {
        base.Awake();

        groundDetector = this.GetOrAddComponent<GroundDetector>();
        rotator = GetComponent<Rotator>();

        CharacterController.gameObject.layer = Layers.Player;
        CharacterController.gameObject.AddComponent<ObjectReference>().SetReference(gameObject);

        // Allows for OnTriggerXXX callbacks
        var capsuleTrigger = CharacterController.gameObject.AddComponent<CapsuleCollider>();
        capsuleTrigger.isTrigger = true;
        capsuleTrigger.height = CharacterController.height;
        capsuleTrigger.radius = CharacterController.radius;
        capsuleTrigger.center = CharacterController.center;
        var rBody = CharacterController.gameObject.AddComponent<Rigidbody>();
        rBody.isKinematic = true;
    }

    protected override Vector3 CalculateVelocity(int time, Keys keys, Vector2 mouse, float deltaTime, out bool jumping)
    {
        jumping = false;
        var previousVelocity = TargetEvent.Velocity;
        var newVelocity = previousVelocity;

        if (TargetEvent.IsGrounded)
        {
            var targetVelocity = Quaternion.Euler(0f, rotator.TargetEvent.Y, 0f) * keys.AsInputVelocity() * moveSpeed;
            var multiplier = previousVelocity.magnitude < targetVelocity.magnitude ? acceleration : deceleration;
            newVelocity = Vector3.Lerp(previousVelocity, targetVelocity, multiplier * deltaTime);

            if (keys.HasFlag(Keys.Move_Jump))
            {
                jumping = true;
                newVelocity.y = jumpHeight;
            }
            else
            {
                newVelocity.y = Physics.gravity.y * deltaTime;
            }
        }
        else
        {
            newVelocity += Physics.gravity * deltaTime;
        }

        return newVelocity;
    }

    protected override void OnGroundedChanged(bool isGrounded)
    {
        groundDetector.SetGrounded(isGrounded);
    }
}