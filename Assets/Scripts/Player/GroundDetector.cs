﻿using UnityEngine;

public class GroundDetector : MonoBehaviour
{
    private const float NudgeDistance = 0.05f;
    private const float HalfNudgeDistance = NudgeDistance / 2f;

    [SerializeField] private LayerMask layerMask = Layers.WalkableLayerMask;
    
    public bool IsGrounded { get; private set; }
    public Vector3 Normal { get; private set; }

    public void SetLayerMask(LayerMask newLayerMask)
    {
        layerMask = newLayerMask;
    }

    public void SetGrounded(bool isGrounded)
    {
        IsGrounded = isGrounded;
    }

    public void SetNormal(Vector3 normal)
    {
        Normal = normal;
    }

    public void Evaluate(Vector3 position, float radius)
    {
        RaycastHit hit;
        var up = transform.up;
        var ray = new Ray(position + up * (radius + HalfNudgeDistance), -up);
        var isGrounded = Physics.SphereCast(ray, radius, out hit, NudgeDistance, layerMask, QueryTriggerInteraction.Ignore);
        SetGrounded(isGrounded);
        SetNormal(isGrounded ? hit.normal : Vector3.up);
    }
}