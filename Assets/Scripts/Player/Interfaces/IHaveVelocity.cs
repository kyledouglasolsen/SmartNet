﻿using UnityEngine;

public interface IHaveVelocity
{
    Vector3 Velocity { get; }
}