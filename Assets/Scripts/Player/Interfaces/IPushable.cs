﻿using UnityEngine;

public interface IPushable
{
    int Priority { get; }
    Vector3 Position { get; }
    Vector3 Velocity { get; }
    bool CanBePushed { get; }

    void AddForce(Vector3 force);
}