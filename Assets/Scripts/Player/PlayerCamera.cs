﻿using SmartNet;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    private CameraTarget target;

    private void Awake()
    {
        IdentityLibrary.PlayerSpawnEvent += identity =>
        {
            if (!identity.IsOwner)
            {
                return;
            }

            target = identity.GetComponent<CameraTarget>();
        };
    }

    private void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        Vector3 position;
        Quaternion rotation;
        target.GetTransformData(out position, out rotation);
        transform.SetPositionAndRotation(position, rotation);
    }
}