﻿using System.Collections.Generic;
using UnityEngine;

public class PushApart : MonoBehaviour
{
    [SerializeField] private Vector3 center;
    [SerializeField] private float pushForce = 1f;

    private IPushable myPushable;
    private readonly List<IPushable> velocities = new List<IPushable>();

    private void Awake()
    {
        myPushable = GetComponent<IPushable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var pushable = FindPushable(other);

        if (pushable != null)
        {
            if (myPushable != null)
            {
                if (myPushable.Priority < pushable.Priority)
                {
                    return;
                }
            }

            if (!velocities.Contains(pushable))
            {
                velocities.Add(pushable);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var pushable = FindPushable(other);

        if (pushable != null)
        {
            velocities.Remove(pushable);
        }
    }

    private void FixedUpdate()
    {
        var from = transform.TransformPoint(center);

        for (var i = velocities.Count - 1; i >= 0; --i)
        {
            if (velocities[i] == null || !velocities[i].CanBePushed)
            {
                velocities.RemoveAt(i);
                continue;
            }

            var direction = velocities[i].Position - from;
            direction.y = 0f;

            if (direction == Vector3.zero)
            {
                direction = Random.onUnitSphere;
                direction.y = 0f;
            }

            velocities[i].AddForce(direction.normalized * pushForce);
        }
    }

    private IPushable FindPushable(Collider other)
    {
        var target = other.GetComponent<IPushable>();

        if (target != null)
        {
            return target;
        }

        var reference = other.GetComponent<ObjectReference>()?.Reference as GameObject;

        if (reference == null || reference == gameObject)
        {
            return null;
        }

        return reference.GetComponent<IPushable>();
    }

    private void OnDrawGizmosSelected()
    {
        DebugExtensions.DrawPoint(transform.TransformPoint(center));
    }
}