﻿using System;
using SmartNet;
using UnityEngine;

public class RigidbodyMover : AuthoritativeComponent<RigidbodyMover.MoverEvent>, IPushable, IHaveVelocity
{
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float jumpHeight = 7f;
    [SerializeField] private float maxVelocityChange = 1f;
    [SerializeField] private float groundedContactHeight = 0.25f;
    [SerializeField] private float idleDrag = 10f;
    [SerializeField] private float movingDrag = 0.25f;
    [SerializeField] private float dragVelocityMultiplier = 0.1f;
    [SerializeField] private LayerMask groundLayerMask = Layers.WalkableLayerMask;

    private DecoupledRigibody decoupledRigidbody;
    private ObjectReference objectReference;
    private CapsuleCollider capsuleCollider;
    private PhysicMaterial physicMaterial;
    private Rotator rotator;
    private GroundDetector groundDetector;

    private Rigidbody Rigidbody => GetAuthoritativeRigidbody();

    public override bool SnapCorrections { get; } = false;

    public int Priority { get; } = 0;
    public Vector3 Position => TargetEvent.Position;
    public Vector3 Velocity => TargetEvent.Velocity;
    public bool CanBePushed => enabled;

    public void AddForce(Vector3 force)
    {
        Rigidbody.velocity += force;
        TargetEvent = new MoverEvent(TargetEvent.Position, TargetEvent.Rotation, TargetEvent.Velocity + force, TargetEvent.Normal, TargetEvent.IsGrounded);
    }

    private void Awake()
    {
        rotator = GetComponent<Rotator>();

        groundDetector = Rigidbody.gameObject.AddComponent<GroundDetector>();
        groundDetector.SetLayerMask(groundLayerMask);

        Rigidbody.gameObject.AddComponent<ObjectReference>().SetReference(gameObject);

        capsuleCollider = Rigidbody.gameObject.AddComponent<CapsuleCollider>();
        capsuleCollider.center = new Vector3(0f, 1f, 0f);
        capsuleCollider.radius = 0.5f;
        capsuleCollider.height = 2f;
        physicMaterial = new PhysicMaterial()
        {
            dynamicFriction = 0f,
            staticFriction = 0f,
            bounciness = 0f,
            bounceCombine = PhysicMaterialCombine.Minimum,
            frictionCombine = PhysicMaterialCombine.Minimum
        };
        capsuleCollider.material = physicMaterial;
    }

    public override Rigidbody GetAuthoritativeRigidbody()
    {
        if (decoupledRigidbody == null)
        {
            decoupledRigidbody = this.GetOrAddComponent<DecoupledRigibody>();
            decoupledRigidbody.TryCreateRigidbody();
        }

        return decoupledRigidbody.Rigidbody;
    }

    protected override void OnEnable()
    {
        TargetEvent = new MoverEvent(transform.localPosition, transform.localRotation, Rigidbody.velocity, Vector3.up, false);

        if (decoupledRigidbody != null)
        {
            decoupledRigidbody.enabled = true;
        }

        ApplyRigidbodySettings();

        base.OnEnable();
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        TargetEvent = new MoverEvent(transform.localPosition, transform.localRotation, Vector3.zero, Vector3.up, false);

        if (decoupledRigidbody != null)
        {
            decoupledRigidbody.enabled = false;
        }
    }

    protected override MoverEvent Simulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        ApplyRigidbodySettings();

        var keyVelocity = keys.AsInputVelocity();
        var drag = 0f;
        var jumped = false;

        if (TargetEvent.IsGrounded)
        {
            var rotation = Quaternion.Euler(0f, rotator.TargetEvent.Y, 0f);
            var targetVelocity = rotation * keyVelocity * moveSpeed * deltaTime;
            var changeInVelocity = targetVelocity - TargetEvent.Velocity;
            changeInVelocity.x = Mathf.Clamp(changeInVelocity.x, -maxVelocityChange, maxVelocityChange);
            changeInVelocity.z = Mathf.Clamp(changeInVelocity.z, -maxVelocityChange, maxVelocityChange);
            changeInVelocity.y = 0f;

            var velocityDot = Vector3.Dot(targetVelocity, TargetEvent.Velocity);
            var velocityMagnitude = TargetEvent.Velocity.magnitude;

            drag = keyVelocity == Vector3.zero || velocityDot <= 0f ? idleDrag : movingDrag;
            drag *= velocityMagnitude * dragVelocityMultiplier;

            Rigidbody.AddForce(changeInVelocity, ForceMode.VelocityChange);

            if (keys.HasFlag(Keys.Move_Jump))
            {
                jumped = true;
                Rigidbody.AddRelativeForce(Vector3.up * jumpHeight, ForceMode.VelocityChange);
            }
        }

        Rigidbody.drag = drag;

        SinglePhysics.Simulate(Rigidbody, deltaTime);

        var position = Rigidbody.position.Round();

        if (jumped)
        {
            groundDetector.SetGrounded(false);
        }
        else
        {
            groundDetector.Evaluate(position, capsuleCollider.radius);
        }

        return new MoverEvent(position, Quaternion.Euler(0f, rotator?.TargetEvent.Y ?? 0f, 0f), Rigidbody.velocity, groundDetector.Normal, groundDetector.IsGrounded);
    }

    public override void ApplyEvent(MoverEvent timelineEvent)
    {
        transform.position = timelineEvent.Position;
        groundDetector.SetGrounded(timelineEvent.IsGrounded);
    }

    protected override void SerializeAuthority(Writer writer)
    {
        writer.Write(TargetEvent.Position);
        writer.Write(TargetEvent.Velocity);
        writer.Write(TargetEvent.IsGrounded);
    }

    protected override MoverEvent DeserializeAuthority(Reader reader)
    {
        return new MoverEvent(reader.ReadVector3(), TargetEvent.Rotation, reader.ReadVector3(), TargetEvent.Normal, reader.ReadBoolean());
    }

    public override int CalculateStateHash()
    {
        return HashCode.Start.Hash(TargetEvent.Position);
    }

    private void ApplyRigidbodySettings()
    {
        Rigidbody.position = TargetEvent.Position;
        Rigidbody.rotation = TargetEvent.Rotation;
        Rigidbody.velocity = TargetEvent.Velocity;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position + transform.up * groundedContactHeight, new Vector3(2f, 0f, 2f));

        DebugExtensions.DrawArrow(transform.position, TargetEvent.Velocity);
    }

    [Serializable]
    public struct MoverEvent : ITimelineEvent<MoverEvent>
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Velocity;
        public Vector3 Normal;
        public bool IsGrounded;

        public MoverEvent(Vector3 position, Quaternion rotation, Vector3 velocity, Vector3 normal, bool isGrounded)
        {
            Position = position;
            Rotation = rotation;
            Velocity = velocity;
            Normal = normal;
            IsGrounded = isGrounded;
        }

        public MoverEvent InterpolateTo(MoverEvent otherEvent, float lerp)
        {
            return new MoverEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Quaternion.Lerp(Rotation, otherEvent.Rotation, lerp), Vector3.Lerp(Velocity, otherEvent.Velocity, lerp),
                Vector3.Lerp(Normal, otherEvent.Normal, lerp), lerp < 1f ? IsGrounded : otherEvent.IsGrounded);
        }
    }
}