﻿using SmartNet;
using UnityEngine;

public class Rotator : AuthoritativeComponent<Rotator.RotatorEvent>
{
    [SerializeField] private float mouseSensitivity = 10f;

    public float MouseSensitivity => mouseSensitivity;

    public override bool SnapCorrections { get; } = false;

    protected override void OnDisable()
    {
        base.OnDisable();

        TargetEvent = new RotatorEvent(0f, transform.localEulerAngles.y);
    }

    protected override RotatorEvent Simulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        var y = Mathf.Repeat(TargetEvent.Y + mouse.x * mouseSensitivity * deltaTime, 360f);
        var x = Mathf.Repeat(TargetEvent.X - mouse.y * mouseSensitivity * deltaTime, 360f);
        return new RotatorEvent(x, y);
    }

    public override void ApplyEvent(RotatorEvent timelineEvent)
    {
        transform.localEulerAngles = new Vector3(0f, timelineEvent.Y, 0f);
    }

    protected override void SerializeAuthority(Writer writer)
    {
        writer.Write(TargetEvent.X);
        writer.Write(TargetEvent.Y);
    }

    protected override RotatorEvent DeserializeAuthority(Reader reader)
    {
        return new RotatorEvent(reader.ReadFloat(), reader.ReadFloat());
    }

    public override int CalculateStateHash()
    {
        return HashCode.Start.Hash(TargetEvent.X).Hash(TargetEvent.Y);
    }

    public struct RotatorEvent : ITimelineEvent<RotatorEvent>
    {
        public readonly float X;
        public readonly float Y;

        public RotatorEvent(float x, float y)
        {
            X = x;
            Y = y;
        }

        public RotatorEvent InterpolateTo(RotatorEvent otherEvent, float lerp)
        {
            return new RotatorEvent(Mathf.LerpAngle(X, otherEvent.X, lerp), Mathf.LerpAngle(Y, otherEvent.Y, lerp));
        }
    }
}