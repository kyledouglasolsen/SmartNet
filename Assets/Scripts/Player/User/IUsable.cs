﻿using SmartNet;

public interface IUsable
{
    NetworkId NetworkId { get; }

    void OnBeginUse(User user);
    void OnEndUse(User user);
}