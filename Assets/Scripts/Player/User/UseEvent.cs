﻿using SmartNet;

public struct UseEvent : ITimelineEvent<UseEvent>
{
    public NetworkId NetworkId;

    public UseEvent(NetworkId networkId)
    {
        NetworkId = networkId;
    }

    public UseEvent InterpolateTo(UseEvent otherEvent, float lerp)
    {
        return new UseEvent(lerp < 1f ? NetworkId : otherEvent.NetworkId);
    }
}