﻿using System;
using System.Collections.Generic;
using SmartNet;
using UnityEngine;

public class User : AuthoritativeComponent<UseEvent>
{
    private static readonly List<IUsable> UsableCache = new List<IUsable>();

    [SerializeField] private LayerMask layerMask = Layers.UsableLayerMask;
    [SerializeField] private float maxUseDistance = 10f;
    private CameraTarget cameraTarget;

    public SmartNetIdentity Using { get; private set; }

    public event Action<SmartNetIdentity> BeginUseEvent = delegate { }, EndUseEvent = delegate { };

    public override bool SnapCorrections { get; } = true;

    public override void ApplyEvent(UseEvent timelineEvent)
    {
        var nowUsing = Identity.KnownIdentities.Get(timelineEvent.NetworkId);
        SetUsingLocal(nowUsing);
    }

    public override int CalculateStateHash()
    {
        return HashCode.Start.Hash(Using != null ? Using.NetworkId : NetworkId.Zero);
    }

    protected override UseEvent Simulate(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        if (!Identity.IsServer || !keys.HasFlag(Keys.Use))
        {
            return TargetEvent;
        }

        if (Using != null)
        {
            SetUsingLocal(null);
            return new UseEvent(NetworkId.Zero);
        }

        Vector3 position;
        Quaternion rotation;
        cameraTarget.GetTransformData(out position, out rotation);

        RaycastHit hit;
        var ray = new Ray(position, rotation * Vector3.forward);

        if (Physics.Raycast(ray, out hit, maxUseDistance, layerMask, QueryTriggerInteraction.Collide))
        {
            var nowUsing = hit.collider.GetComponentInParent<SmartNetIdentity>();

            if (nowUsing != null)
            {
                nowUsing.GetComponents(UsableCache);
                nowUsing = UsableCache.Count > 0 ? nowUsing : null;
            }

            SetUsingLocal(nowUsing);
        }

        return new UseEvent(Using != null ? Using.NetworkId : NetworkId.Zero);
    }

    protected override void SerializeAuthority(Writer writer)
    {
        writer.Write(Using != null ? Using.NetworkId : NetworkId.Zero);
    }

    protected override UseEvent DeserializeAuthority(Reader reader)
    {
        return new UseEvent(reader.ReadNetworkId());
    }

    private void Awake()
    {
        cameraTarget = GetComponent<CameraTarget>();
    }

    private void SetUsingLocal(SmartNetIdentity nowUsing)
    {
        if (Using == nowUsing)
        {
            return;
        }

        var wasUsing = Using;
        Using = nowUsing;

        if (Using != null)
        {
            BeginUseEvent(Using);

            Using.GetComponents(UsableCache);

            for (var i = 0; i < UsableCache.Count; ++i)
            {
                UsableCache[i].OnBeginUse(this);
            }
        }
        else
        {
            wasUsing.GetComponents(UsableCache);

            EndUseEvent(wasUsing);

            for (var i = 0; i < UsableCache.Count; ++i)
            {
                UsableCache[i].OnEndUse(this);
            }
        }
    }
}