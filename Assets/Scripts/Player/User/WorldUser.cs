﻿using SmartNet;

public class WorldUser : WorldComponent<UseEvent>
{
    private User user;

    public override bool InterpolateOwner { get; } = false;

    public override UseEvent GetEvent()
    {
        return new UseEvent(user.Using != null ? user.Using.NetworkId : NetworkId.Zero);
    }

    protected override void OnSerializeWorldComponent(Writer writer)
    {
        writer.Write(user.Using != null ? user.Using.NetworkId : NetworkId.Zero);
    }

    protected override UseEvent OnDeserializeWorldComponent(Reader reader)
    {
        return new UseEvent(reader.ReadNetworkId());
    }

    protected override void OnApplyEvent(UseEvent useEvent)
    {
        user.ApplyEvent(useEvent);
    }

    private void Awake()
    {
        user = GetComponent<User>();
    }
}