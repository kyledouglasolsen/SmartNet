﻿public interface IHistoryEventInterpolate
{
    void ApplyState(int time, bool forceHandler = false);
}