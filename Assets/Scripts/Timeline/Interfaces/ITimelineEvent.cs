﻿public interface ITimelineEvent<T>
{
    T InterpolateTo(T otherEvent, float lerp);
}