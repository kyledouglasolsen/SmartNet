﻿using System;
using UnityEngine;

[Serializable]
public abstract class Seat
{
    [SerializeField] private Transform seatTransform;
    [SerializeField] private Vector3 exitPosition;

    public User Occupant { get; private set; }
    public Vector3 WorldExitPosition => seatTransform?.TransformPoint(exitPosition) ?? Vector3.zero;

    public void SetOccupant(User user)
    {
        if (Occupant != null && user == null)
        {
            Occupant.transform.SetParent(null);
            Occupant.transform.position = WorldExitPosition;

            SetComponentsEnabled(Occupant, true);
            
            var col = Occupant.GetComponent<Collider>();
            if(col != null)
            {
                col.enabled = true;
            }
        }

        Occupant = user;

        if (Occupant != null)
        {
            var col = Occupant.GetComponent<Collider>();
            if(col != null)
            {
                col.enabled = false;
            }
            
            Occupant.transform.SetParent(seatTransform);
            UpdateOccupantPosition();

            SetComponentsEnabled(Occupant, false);
            UpdateOccupantPosition();
        }
    }

    public void UpdateOccupantPosition()
    {
        if (Occupant != null)
        {
            Occupant.transform.localPosition = Vector3.zero;
            Occupant.transform.localEulerAngles = Vector3.zero;
        }
    }

    public void OnDrawGizmos()
    {
        if (seatTransform == null)
        {
            return;
        }

        var sitPosition = seatTransform.position;
        var worldExitPosition = WorldExitPosition;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(seatTransform.position, 0.5f);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(worldExitPosition, 0.5f);
        Gizmos.color = Color.white;
        Gizmos.DrawLine(sitPosition, worldExitPosition);
    }

    protected abstract void SetComponentsEnabled(User user, bool isEnabled);
}