﻿using UnityEngine;

public class Vehicle : ClientAuthoritativeRigidbody, IPushable, IHaveVelocity
{
    [SerializeField] private float maxAngle = 30f;
    [SerializeField] private float maxTorque = 900f;
    [SerializeField] private Transform wheelColliderParent = null, centerOfGravity = null;
    [SerializeField] private Wheel frontLeftWheel = null, frontRightWheel = null, backLeftWheel = null, backRightWheel = null;

    public float Steer { get; private set; }
    public float Throttle { get; private set; }

    public int Priority { get; } = 1;
    public Vector3 Position => TargetEvent.Position;
    public Vector3 Velocity => TargetEvent.Velocity;
    public bool CanBePushed => enabled;

    public void AddForce(Vector3 force)
    {
        Rigidbody.velocity += force;
        TargetEvent = new RigidbodyEvent(TargetEvent.Position, TargetEvent.Rotation, TargetEvent.Velocity + force, TargetEvent.AngularVelocity);
    }

    public void ApplySteerAndThrottle(float steer, float throttle)
    {
        Steer = steer;
        Throttle = throttle;

        frontLeftWheel.SetSteer(Steer);
        frontRightWheel.SetSteer(Steer);
        backLeftWheel.SetTorque(Throttle);
        backRightWheel.SetTorque(Throttle);
    }

    protected override void ApplyRigidbodyForces(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        var keyVelocity = keys.AsInputVelocity();
        ApplySteerAndThrottle(keyVelocity.x * maxAngle, keyVelocity.z * maxTorque);
    }

    protected override void Awake()
    {
        base.Awake();

        var rBody = GetAuthoritativeRigidbody();
        rBody.centerOfMass = centerOfGravity.localPosition;

        rBody.gameObject.AddComponent<ObjectReference>().SetReference(gameObject);

        var localPosition = wheelColliderParent.localPosition;
        var localRotation = wheelColliderParent.localRotation;
        wheelColliderParent.SetParent(rBody.transform);
        wheelColliderParent.localPosition = localPosition;
        wheelColliderParent.localRotation = localRotation;
        wheelColliderParent.gameObject.SetActive(true);
    }
}