﻿using System;
using System.Linq;
using SmartNet;
using UnityEngine;

public class VehicleSeats : MonoBehaviour, IUsable
{
    [SerializeField] private VehicleSeat[] seats;
    private Vehicle vehicle;
    private SmartNetIdentity identity;

    public NetworkId NetworkId => identity.NetworkId;

    public void OnBeginUse(User user)
    {
        var seat = seats.FirstOrDefault(x => x.Occupant == null);

        if (seat == null)
        {
            Debug.LogError("No free seats available");
            return;
        }

        seat.SetOccupant(user);

        if (seat.DriverSeat)
        {
            identity.ReplaceOwner(user.Identity.Owner);
        }
    }

    public void OnEndUse(User user)
    {
        var seat = seats.FirstOrDefault(x => x.Occupant == user);

        if (seat == null)
        {
            Debug.LogError("User not found in seat!");
            return;
        }

        seat.SetOccupant(null);

        if (seat.DriverSeat)
        {
            identity.RemoveOwner();
        }
    }

    private void Start()
    {
        vehicle = GetComponent<Vehicle>();
        identity = GetComponent<SmartNetIdentity>();

        for (var i = 0; i < seats.Length; ++i)
        {
            seats[i].SetVehicle(vehicle);
        }
    }

    private void OnDrawGizmosSelected()
    {
        for (var i = 0; i < seats.Length; ++i)
        {
            seats[i]?.OnDrawGizmos();
        }
    }
}

[Serializable]
internal class VehicleSeat : Seat
{
    [SerializeField] private bool driverSeat;
    private Vehicle vehicle;

    public bool DriverSeat => driverSeat;

    protected override void SetComponentsEnabled(User user, bool isEnabled)
    {
        var rbMover = user.GetComponent<RigidbodyMover>();
        var mover = user.GetComponent<CharacterControllerMover>();

        if (rbMover != null)
        {
            if (isEnabled)
            {
                rbMover.AddForce(vehicle.GetAuthoritativeRigidbody().velocity);
            }
            
            rbMover.enabled = isEnabled;
        }

        if (mover != null)
        {
            if(isEnabled)
            {
                mover.AddForce(vehicle.GetAuthoritativeRigidbody().velocity);
            }
            
            mover.enabled = isEnabled;
        }
    }

    public void SetVehicle(Vehicle newVehicle)
    {
        vehicle = newVehicle;
    }
}