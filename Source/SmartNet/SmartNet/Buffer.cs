﻿using Debug = UnityEngine.Debug;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SmartNet
{
    internal class Buffer
    {
        private uint zero;
        private byte[] localBuffer;

        private const int InitialSize = 64;
        private const float GrowthFactor = 1.5f;
        private const int BufferSizeWarning = 1024 * 1024 * 128;

        public uint Position { get; private set; }

        public Buffer()
        {
            localBuffer = new byte[InitialSize];
        }

        public Buffer(byte[] buffer)
        {
            localBuffer = buffer;
        }

        public Buffer(ArraySegment<byte> buffer)
        {
            localBuffer = buffer.Array;
            Position = (uint)buffer.Offset;
            zero = Position;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte ReadByte()
        {
            if (Position >= localBuffer.Length)
            {
                throw new IndexOutOfRangeException($"ReadByte out of range: {this}");
            }

            return localBuffer[Position++];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ReadBytes(byte[] buffer, uint count)
        {
            if (Position + count > localBuffer.Length)
            {
                throw new IndexOutOfRangeException($"ReadBytes out of range: ({count}) {this}");
            }

            for (var i = 0; i < count; i++)
            {
                buffer[i] = localBuffer[Position + i];
            }

            Position += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ReadChars(char[] buffer, uint count)
        {
            if (Position + count > localBuffer.Length)
            {
                throw new IndexOutOfRangeException($"ReadChars out of range: ({count}) {this}");
            }

            for (var i = 0; i < count; i++)
            {
                buffer[i] = (char)localBuffer[Position + i];
            }

            Position += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ReadBuffer(Buffer buffer, uint count)
        {
            if (Position + count > localBuffer.Length)
            {
                throw new IndexOutOfRangeException($"ReadBuffer out of range: ({count}) {this}");
            }

            buffer.WriteCheckForSpace(count);

            for (var i = 0; i < count; i++)
            {
                buffer.localBuffer[buffer.Position + i] = localBuffer[Position + 1];
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] AsArray()
        {
            return localBuffer;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> AsSegment()
        {
            return new ArraySegment<byte>(localBuffer, 0, localBuffer.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetReadBytesAsSegment()
        {
            return new ArraySegment<byte>(localBuffer, 0, (int)Position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetRemainingBytesAsSegment()
        {
            return new ArraySegment<byte>(localBuffer, (int)Position, localBuffer.Length - (int)Position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteByte(byte value)
        {
            WriteCheckForSpace(1);
            localBuffer[Position] = value;
            Position += 1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteByte2(byte value0, byte value1)
        {
            WriteCheckForSpace(2);
            localBuffer[Position] = value0;
            localBuffer[Position + 1] = value1;
            Position += 2;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteByte4(byte value0, byte value1, byte value2, byte value3)
        {
            WriteCheckForSpace(4);
            localBuffer[Position] = value0;
            localBuffer[Position + 1] = value1;
            localBuffer[Position + 2] = value2;
            localBuffer[Position + 3] = value3;
            Position += 4;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteByte8(byte value0, byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7)
        {
            WriteCheckForSpace(8);
            localBuffer[Position] = value0;
            localBuffer[Position + 1] = value1;
            localBuffer[Position + 2] = value2;
            localBuffer[Position + 3] = value3;
            localBuffer[Position + 4] = value4;
            localBuffer[Position + 5] = value5;
            localBuffer[Position + 6] = value6;
            localBuffer[Position + 7] = value7;
            Position += 8;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBytesAtOffset(byte[] buffer, uint targetOffset, uint count)
        {
            var newEnd = count + targetOffset;

            WriteCheckForSpace(newEnd);

            if (targetOffset == 0 && count == buffer.Length)
            {
                buffer.CopyTo(localBuffer, Position);
            }
            else
            {
                for (var i = 0; i < count; i++)
                {
                    localBuffer[targetOffset + i] = buffer[i];
                }
            }

            if (newEnd > Position)
            {
                Position = newEnd;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBytes(byte[] buffer, uint count)
        {
            WriteCheckForSpace(count);

            if (count == buffer.Length)
            {
                buffer.CopyTo(localBuffer, Position);
            }
            else
            {
                for (var i = 0; i < count; i++)
                {
                    localBuffer[Position + i] = buffer[i];
                }
            }

            Position += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBuffer(Buffer buffer)
        {
            WriteBytes(buffer.localBuffer, buffer.Position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void WriteCheckForSpace(uint count)
        {
            if (Position + count < localBuffer.Length)
            {
                return;
            }

            var newLen = (int)(localBuffer.Length * GrowthFactor);

            while (Position + count >= newLen)
            {
                newLen = (int)(newLen * GrowthFactor);

                if (newLen > BufferSizeWarning)
                {
                    Debug.LogWarning($"Buffer size is {newLen} bytes!");
                }
            }

            Array.Resize(ref localBuffer, newLen);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SeekZero()
        {
            Position = zero;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void MovePosition(uint count)
        {
            Position += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void FinishMessage()
        {
            var size = (ushort)(Position - (sizeof(ushort) * 2));
            localBuffer[0] = (byte)(size & 0xff);
            localBuffer[1] = (byte)((size >> 8) & 0xff);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(byte[] buffer)
        {
            localBuffer = buffer;
            Position = 0u;
            zero = 0u;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(Buffer buffer)
        {
            localBuffer = buffer.localBuffer;
            Position = buffer.Position;
            zero = Position;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(ArraySegment<byte> bytes)
        {
            localBuffer = bytes.Array;
            Position = (uint)bytes.Offset;
            zero = Position;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint ZigZagEncode(int value)
        {
            return (uint)((value << 1) ^ (value >> 31));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ulong ZigZagEncode(long value)
        {
            return (ulong)((value << 1) ^ (value >> 63));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ZigZagDecode(uint value)
        {
            return (int)((value >> 1) ^ (~(value & 1) + 1));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long ZigZagDecode(ulong value)
        {
            return (long)((value >> 1) ^ (~(value & 1) + 1));
        }

        public override string ToString()
        {
            return $"Buffer size:{localBuffer.Length} position:{Position}";
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct UIntFloat
    {
        [FieldOffset(0)] public float floatValue;
        [FieldOffset(0)] public int intValue;
        [FieldOffset(0)] public uint uIntValue;
        [FieldOffset(0)] public double doubleValue;
        [FieldOffset(0)] public long longValue;
        [FieldOffset(0)] public ulong uLongValue;
    }

    internal class FloatConversion
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ToSingle(int value)
        {
            return new UIntFloat {intValue = value}.floatValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ToSingle(uint value)
        {
            return new UIntFloat {uIntValue = value}.floatValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double ToDouble(long value)
        {
            return new UIntFloat {longValue = value}.doubleValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double ToDouble(ulong value)
        {
            return new UIntFloat {uLongValue = value}.doubleValue;
        }
    }
}