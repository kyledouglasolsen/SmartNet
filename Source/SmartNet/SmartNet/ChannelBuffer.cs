﻿using Debug = UnityEngine.Debug;
using Time = UnityEngine.Time;
using System;
using System.Collections.Generic;

namespace SmartNet
{
    internal class ChannelBuffer : IDisposable
    {
        private readonly Connection connection;
        private ChannelPacket currentPacket;

        private float lastFlushTime;

        private byte channelId;
        private int maxPacketSize;
        private bool isReliable;
        private bool isBroken;
        private int maxPendingPacketCount;

        private const int MaxFreePacketCount = 512; //  this is for all connections. maybe make this configurable
        private const int MaxPendingPacketCount = 16; // this is per connection. each is around 1400 bytes (MTU)

        private Queue<ChannelPacket> pendingPackets;
        private static List<ChannelPacket> freePackets;
        internal static int PendingPacketCount; // this is across all connections. only used for profiler metrics.

        public float MaxDelay = 0.01f;

        private const int PacketHeaderReserveSize = 100;

        public ChannelBuffer(Connection connection, int bufferSize, byte cid, bool isReliable)
        {
            this.connection = connection;
            maxPacketSize = bufferSize - PacketHeaderReserveSize;
            currentPacket = new ChannelPacket(maxPacketSize, isReliable);

            channelId = cid;
            maxPendingPacketCount = MaxPendingPacketCount;
            this.isReliable = isReliable;
            if (isReliable)
            {
                pendingPackets = new Queue<ChannelPacket>();
                if (freePackets == null)
                {
                    freePackets = new List<ChannelPacket>();
                }
            }
        }

        public void CheckInternalBuffer()
        {
            if (Time.realtimeSinceStartup - lastFlushTime < MaxDelay || currentPacket.IsEmpty())
            {
                return;
            }

            SendInternalBuffer();
            lastFlushTime = Time.realtimeSinceStartup;
        }

        public bool SendInternalBuffer()
        {
            if (!isReliable || pendingPackets.Count <= 0)
            {
                return currentPacket.SendToTransport(connection, channelId);
            }

            while (pendingPackets.Count > 0)
            {
                var packet = pendingPackets.Dequeue();

                if (!packet.SendToTransport(connection, channelId))
                {
                    pendingPackets.Enqueue(packet);
                    break;
                }

                PendingPacketCount -= 1;
                FreePacket(packet);

                if (!isBroken || pendingPackets.Count >= maxPendingPacketCount / 2)
                {
                    continue;
                }

                Debug.LogWarning("ChannelBuffer recovered from overflow but data was lost.");
                isBroken = false;
            }

            return true;
        }

        public bool Send(Writer writer)
        {
            return Send(writer.AsArray(), (int)writer.Position);
        }

        public bool Send(byte[] bytes, int bytesToSend)
        {
            if (bytesToSend <= 0)
            {
                Debug.LogError("ChannelBuffer:SendBytes cannot send zero bytes");
                return false;
            }

            if (bytesToSend > maxPacketSize)
            {
                Debug.LogError($"Failed to send big message of {bytesToSend} bytes. The maximum is {maxPacketSize} bytes on this channel.");
                return false;
            }

            if (!currentPacket.HasSpace(bytesToSend))
            {
                if (isReliable)
                {
                    if (pendingPackets.Count == 0)
                    {
                        if (!currentPacket.SendToTransport(connection, channelId))
                        {
                            QueuePacket();
                        }

                        currentPacket.Write(bytes, bytesToSend);
                        return true;
                    }

                    if (pendingPackets.Count >= maxPendingPacketCount)
                    {
                        if (!isBroken)
                        {
                            Debug.LogError($"ChannelBuffer buffer limit of {pendingPackets.Count} packets reached.");
                        }

                        isBroken = true;
                        return false;
                    }

                    QueuePacket();
                    currentPacket.Write(bytes, bytesToSend);
                    return true;
                }

                if (!currentPacket.SendToTransport(connection, channelId))
                {
                    Debug.Log($"ChannelBuffer SendBytes no space on unreliable channel {channelId}");
                    return false;
                }

                currentPacket.Write(bytes, bytesToSend);
                return true;
            }

            currentPacket.Write(bytes, bytesToSend);

            if (MaxDelay <= 0f)
            {
                return SendInternalBuffer();
            }

            return true;
        }

        private void QueuePacket()
        {
            PendingPacketCount += 1;
            pendingPackets.Enqueue(currentPacket);
            currentPacket = AllocPacket();
        }

        private ChannelPacket AllocPacket()
        {
            if (freePackets.Count == 0)
            {
                return new ChannelPacket(maxPacketSize, isReliable);
            }

            var packet = freePackets[freePackets.Count - 1];
            freePackets.RemoveAt(freePackets.Count - 1);

            packet.Reset();
            return packet;
        }

        private static void FreePacket(ChannelPacket packet)
        {
            if (freePackets.Count >= MaxFreePacketCount)
            {
                return;
            }

            freePackets.Add(packet);
        }

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                if (pendingPackets != null)
                {
                    while (pendingPackets.Count > 0)
                    {
                        PendingPacketCount -= 1;

                        var packet = pendingPackets.Dequeue();

                        if (freePackets.Count < MaxFreePacketCount)
                        {
                            freePackets.Add(packet);
                        }
                    }

                    pendingPackets.Clear();
                }
            }

            disposed = true;
        }
    }
}