﻿using SerializeField = UnityEngine.SerializeField;
using System;

namespace SmartNet
{
    [Serializable]
    public class ChannelQos
    {
        [SerializeField] private QosType type;

        public QosType Qos => type;

        public ChannelQos(QosType value)
        {
            type = value;
        }

        public ChannelQos()
        {
            type = QosType.Unreliable;
        }

        public ChannelQos(ChannelQos channel)
        {
            if (channel == null)
            {
                throw new NullReferenceException("channel is not defined");
            }

            type = channel.type;
        }
    }
}