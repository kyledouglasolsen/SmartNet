﻿using Debug = UnityEngine.Debug;
using NetworkError = UnityEngine.Networking.NetworkError;
using NetworkEventType = UnityEngine.Networking.NetworkEventType;
using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using System;
using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    public class Client
    {
        private static readonly List<Client> Clients = new List<Client>();

        private readonly MessageHandlers handlers = new MessageHandlers();
        private readonly byte[] buffer = new byte[ushort.MaxValue];
        private const int MaxEventsPerFrame = 1024;
        private int clientId;
        private ServerSettings serverSettings;

        public string Ip { get; internal set; }
        public int Port { get; private set; }
        public Connection Connection { get; private set; }
        public ConnectState State { get; internal set; }

        public event Action ConnectEvent = delegate { }, DisconnectEvent = delegate { };

        public Client()
        {
            NetworkTransport.Init();
            Updater.Init();
            Configure(serverSettings = serverSettings ?? Default.ServerSettings);
        }

        public void Configure(ServerSettings settings)
        {
            serverSettings = settings;
        }

        public void Connect(string ip, int port)
        {
            RegisterDefaultHandlers();

            Clients.Add(this);
            clientId = NetworkTransport.AddHost(serverSettings.ToHostTopology(), 0);
            Helpers.GetIpOrHostname(this, ip);
            Port = port;
        }

        public void Disconnect()
        {
            if (clientId != -1)
            {
                NetworkTransport.RemoveHost(clientId);
                clientId = -1;
            }

            Clients.Remove(this);
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            RegisterHandler(MessageTypes.GetTypeId(typeof(T)), info =>
            {
                var generic = GenericMessageInfo<T>.Pop(info.Type, info.Connection, info.ReadMessage<T>(), info.IsServer);
                handler(generic);
                GenericMessageInfo<T>.Push(generic);
            });
        }

        public void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            handlers.Register(messageType, handler);
        }

        public void UnregisterHandler<T>() where T : INetMessage
        {
            UnregisterHandler(MessageTypes.GetTypeId(typeof(T)));
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public bool HasHandler<T>() where T : INetMessage
        {
            return handlers.HasHandler<T>();
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.HasHandler(messageType);
        }

        public bool Send<T>(T message, int channelId) where T : INetMessage
        {
            return Send(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool Send(INetMessage message, int channelId)
        {
            return Send(MessageTypes.GetTypeId(message.GetType()), message, channelId);
        }

        public bool Send(uint messageType, INetMessage message, int channelId)
        {
            return CanSend() && Connection.Send(messageType, message, channelId);
        }

        public bool Send(Writer writer, int channelId)
        {
            return CanSend() && Connection.Send(writer, channelId);
        }

        public bool Send(byte[] bytes, int count, int channelId)
        {
            return CanSend() && Connection.Send(bytes, count, channelId);
        }

        protected virtual void RegisterDefaultHandlers()
        {
            RegisterHandler<OwnerSpawn>(OwnerSpawn.HandleMessage);
            RegisterHandler<WorldSpawn>(WorldSpawn.HandleMessage);
            RegisterHandler<WorldDestroy>(WorldDestroy.HandleMessage);
            RegisterHandler<SetOwner>(SetOwner.HandleMessage);
            RegisterHandler<RemoveOwner>(RemoveOwner.HandleMessage);
            RegisterHandler<UpdateTime>(NetworkTime.ReceiveTimeFromServer);
            RegisterHandler(MessageTypes.GetTypeId<IdentityMessage>(), IdentityMessage.HandleMessage);
        }

        internal bool CanSend()
        {
            if (Connection != null)
            {
                if (State != ConnectState.Connected)
                {
                    Debug.LogError("Client SendBytes when not connected to a server");
                    return false;
                }

                return true;
            }

            Debug.LogError("Unable to send without a connection");
            return false;
        }

        public static void UpdateAll()
        {
            for (var i = 0; i < Clients.Count; ++i)
            {
                Clients[i].Update();
            }
        }

        internal void Update()
        {
            if (clientId == -1)
            {
                return;
            }

            switch (State)
            {
                case ConnectState.None:
                case ConnectState.Resolving:
                case ConnectState.Disconnected: return;

                case ConnectState.Connecting:
                case ConnectState.Connected: break;

                case ConnectState.Failed:
                    Debug.LogError($"Client {clientId} error on connect {NetworkError.DNSFailure}");
                    State = ConnectState.Disconnected;
                    DisconnectEvent();
                    return;

                case ConnectState.Resolved:
                    
                    State = ConnectState.Connecting;
                    ContinueConnect();
                    return;
            }

            NetworkEventType networkEvent;
            do
            {
                var numEvents = 0;
                networkEvent = NetworkTransport.ReceiveFromHost(clientId, out int connectionId, out int channelId, buffer, buffer.Length, out int receivedSize, out byte errorByte);
                var error = (NetworkError)errorByte;

                if (error != NetworkError.Ok)
                {
                    return;
                }

                switch (networkEvent)
                {
                    case NetworkEventType.ConnectEvent:
                        State = ConnectState.Connected;
                        ConnectEvent();
                        break;

                    case NetworkEventType.DataEvent:
                        Connection.TransportReceive(buffer, receivedSize, channelId);
                        break;

                    case NetworkEventType.DisconnectEvent:
                        State = ConnectState.Disconnected;
                        DisconnectEvent();
                        break;

                    case NetworkEventType.Nothing: break;
                    default:
                        Debug.LogError($"Unknown network message type received: {networkEvent}");
                        break;
                }

                if (clientId == -1 || ++numEvents >= MaxEventsPerFrame)
                {
                    break;
                }
            } while (networkEvent != NetworkEventType.Nothing);

            if (Connection != null && State == ConnectState.Connected)
            {
                Connection.FlushChannels();
            }
        }

        internal void ContinueConnect()
        {
            var connectionId = NetworkTransport.Connect(clientId, Ip, Port, 0, out byte error);
            var connections = new ConnectionList();
            Connection = new Connection(Ip, clientId, connectionId, serverSettings, handlers, new KnownIdentities(), connections, true, false);
            connections.Add(Connection);
        }
    }
}