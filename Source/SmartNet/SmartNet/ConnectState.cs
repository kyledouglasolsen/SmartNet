﻿namespace SmartNet
{
    public enum ConnectState
    {
        None,
        Resolving,
        Resolved,
        Connecting,
        Connected,
        Disconnected,
        Failed
    }
}
