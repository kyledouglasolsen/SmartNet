﻿using Debug = UnityEngine.Debug;
using NetworkError = UnityEngine.Networking.NetworkError;
using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using Object = UnityEngine.Object;
using System;
using System.Collections.Generic;
using SmartNet.Messages;
using SmartNet.Profiler;

namespace SmartNet
{
    public class Connection : IDisposable
    {
        private readonly bool isServer;
        private readonly ChannelBuffer[] channels;
        private readonly MessageHandlers handlers;
        private readonly MessageInfo messageInfo = new MessageInfo();
        private readonly Writer messageWriter = new Writer();
        private readonly PooledStack<Reader> readers = new PooledStack<Reader>(() => new Reader(), 64);
        private readonly HashSet<SmartNetIdentity> visible = new HashSet<SmartNetIdentity>();
        private readonly HashSet<Connection> observers = new HashSet<Connection>();

        public readonly KnownIdentities KnownIdentities;
        public readonly ConnectionList KnownConnections;

        public readonly string Ip;
        public readonly int HostId;
        public readonly int ConnectionId;
        public readonly bool IsMe;
        public SmartNetIdentity Player { get; private set; }
        public IEnumerable<Connection> Observers => observers;

        public Connection(string ip, int hostId, int connectionId, ServerSettings serverSettings, MessageHandlers handlers, KnownIdentities knownIdentities, ConnectionList knownConnections, bool isMe,
            bool isServer)
        {
            Ip = ip;
            HostId = hostId;
            ConnectionId = connectionId;
            IsMe = isMe;
            this.handlers = handlers ?? new MessageHandlers();
            KnownIdentities = knownIdentities;
            KnownConnections = knownConnections;
            this.isServer = isServer;

            var channelCount = serverSettings.DefaultConfig.ChannelCount;
            var packetSize = serverSettings.DefaultConfig.PacketSize;

            channels = new ChannelBuffer[channelCount];

            for (var i = 0; i < channelCount; i++)
            {
                var qos = serverSettings.DefaultConfig.Channels[i];
                int actualPacketSize = packetSize;

                if ((int)qos.Qos == (int)QosType.ReliableFragmented || (int)qos.Qos == (int)QosType.UnreliableFragmented)
                {
                    actualPacketSize = serverSettings.DefaultConfig.FragmentSize * 128;
                }

                channels[i] = new ChannelBuffer(this, actualPacketSize, (byte)i, Helpers.IsReliableQoS(qos.Qos));
            }
        }

        public void Disconnect()
        {
            if (HostId == -1)
            {
                return;
            }

            if (Player != null)
            {
                KnownIdentities.Remove(Player.NetworkId);
                Object.Destroy(Player.gameObject);
            }

            NetworkTransport.Disconnect(HostId, ConnectionId, out byte error);
        }

        public void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            handlers.Register(messageType, handler);
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.HasHandler(messageType);
        }

        public bool InvokeHandlerNoData(uint messageType)
        {
            return InvokeHandler(messageType, null, 0, 0);
        }

        public bool InvokeHandler(uint messageType, Reader reader, uint messageSize, int channelId)
        {
            messageInfo.Initialize(messageType, this, reader, messageSize, channelId, isServer);
            return InvokeHandler(messageInfo);
        }

        public bool InvokeHandler(MessageInfo info)
        {
            var handler = handlers.Get(info.Type);

            if (handler == null)
            {
                var type = MessageTypes.GetType(info.Type);
                Debug.LogError(type != null ? $"No handler for builtin messageType {type}" : $"No handler for messageType {info.Type}");
                return false;
            }

            handler.Invoke(info);
            return true;
        }

        private static WorldSpawn worldSpawn = new WorldSpawn();
        private static uint worldSpawnMessageId = MessageTypes.GetTypeId<WorldSpawn>();
        private static OwnerSpawn ownerSpawn = new OwnerSpawn();
        private static uint ownerSpawnMessageId = MessageTypes.GetTypeId<OwnerSpawn>();

        public bool AddVisible(SmartNetIdentity identity)
        {
            if (identity == null)
            {
                return false;
            }

            if (identity.Owner != null)
            {
                observers.Add(identity.Owner);
            }

            var added = visible.Add(identity);

            if (added)
            {
                if (identity.Owner == this)
                {
                    ownerSpawn.Initialize(identity);
                    Send(ownerSpawnMessageId, ownerSpawn, 0);
                }
                else
                {
                    worldSpawn.Initialize(identity);
                    Send(worldSpawnMessageId, worldSpawn, 0);
                }
            }

            return added;
        }

        private static WorldDestroy worldDestroy = new WorldDestroy();
        private static uint worldDestroyId = MessageTypes.GetTypeId<WorldDestroy>();

        public bool RemoveVisible(SmartNetIdentity identity)
        {
            if (identity == null)
            {
                return false;
            }

            if (identity.Owner != null)
            {
                observers.Remove(identity.Owner);
            }

            var removed = visible.Remove(identity);

            if (removed)
            {
                worldDestroy.NetworkId = identity.NetworkId;
                Send(worldDestroyId, worldDestroy, 0);
            }

            return removed;
        }

        public bool HasVisible(SmartNetIdentity identity)
        {
            return visible.Contains(identity);
        }

        public void SetPlayer(SmartNetIdentity identity)
        {
            Player = identity;
        }

        public void FlushChannels()
        {
            if (channels == null)
            {
                return;
            }

            for (var i = 0; i < channels.Length; ++i)
            {
                channels[i].CheckInternalBuffer();
            }
        }

        public bool Send(INetMessage message, int channelId)
        {
            uint byteCount;
            return Send(message, channelId, out byteCount);
        }

        public bool Send(INetMessage message, int channelId, out uint byteCount)
        {
            return Send(MessageTypes.GetTypeId(message.GetType()), message, channelId, out byteCount);
        }

        public bool Send<T>(T message, int channelId) where T : INetMessage
        {
            uint byteCount;
            return Send(message, channelId, out byteCount);
        }

        public bool Send<T>(T message, int channelId, out uint byteCount) where T : INetMessage
        {
            messageWriter.StartMessage<T>();
            message.OnSerialize(messageWriter);
            messageWriter.FinishMessage();

            byteCount = messageWriter.Position;

            Statistics.Add<T>(NetworkDirection.Outbound, byteCount);

            return CheckChannel(channelId) && channels[channelId].Send(messageWriter);
        }

        public bool Send(uint messageType, INetMessage message, int channelId)
        {
            uint byteCount;
            return Send(messageType, message, channelId, out byteCount);
        }

        public bool Send(uint messageType, INetMessage message, int channelId, out uint byteCount)
        {
            messageWriter.StartMessage(messageType);
            message.OnSerialize(messageWriter);
            messageWriter.FinishMessage();

            byteCount = messageWriter.Position;

            Statistics.Add(messageType, NetworkDirection.Outbound, byteCount);

            return CheckChannel(channelId) && channels[channelId].Send(messageWriter);
        }

        public bool Send(byte[] bytes, int count, int channelId)
        {
            return CheckChannel(channelId) && channels[channelId].Send(bytes, count);
        }

        public bool Send(Writer writer, int channelId)
        {
            return CheckChannel(channelId) && channels[channelId].Send(writer);
        }

        public bool TransportSend(byte[] bytes, int count, int channelId, out NetworkError error)
        {
            var sent = NetworkTransport.Send(HostId, ConnectionId, channelId, bytes, count, out byte errorByte);
            error = (NetworkError)errorByte;

            if (sent)
            {
                Statistics.AddToBandwidth(NetworkDirection.Outbound, (uint)count);
            }

            return sent;
        }

        public void TransportReceive(byte[] bytes, int count, int channelId)
        {
            var reader = readers.Pop();
            reader.Replace(bytes);
            HandleReader(reader, count, channelId);
            readers.Push(reader);

            Statistics.AddToBandwidth(NetworkDirection.Inbound, (uint)count);
        }

        protected void HandleReader(Reader reader, int count, int channelId)
        {
            while (reader.Position < count)
            {
                var messageSize = (uint)reader.ReadUShort();
                var messageType = (uint)reader.ReadUShort();
                var messageReader = readers.Pop();
                messageReader.Replace(reader);

                Statistics.Add(messageType, NetworkDirection.Inbound, messageSize);

                try
                {
                    InvokeHandler(messageType, messageReader, messageSize, channelId);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
                finally
                {
                    reader.MovePosition((uint)messageSize);
                    readers.Push(messageReader);
                }
            }
        }

        private bool CheckChannel(int channelId)
        {
            if (channels == null)
            {
                Debug.LogError($"Channels not initialized sending on id {channelId}");
                return false;
            }

            if (channelId < 0 || channelId >= channels.Length)
            {
                Debug.LogError($"Invalid channel when sending buffered data, {channelId}. Current channel count is {channels.Length}");
                return false;
            }

            return true;
        }

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && channels != null)
            {
                for (var i = 0; i < channels.Length; i++)
                {
                    channels[i].Dispose();
                }
            }

            disposed = true;
        }

        ~Connection()
        {
            Dispose(false);
        }
    }
}