﻿namespace SmartNet
{
    public enum ConnectionAcksType
    {
        Acks32 = UnityEngine.Networking.ConnectionAcksType.Acks32,
        Acks64 = UnityEngine.Networking.ConnectionAcksType.Acks64,
        Acks96 = UnityEngine.Networking.ConnectionAcksType.Acks96,
        Acks128 = UnityEngine.Networking.ConnectionAcksType.Acks128,
    }
}