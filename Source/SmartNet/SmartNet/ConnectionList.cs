﻿using System.Collections.Generic;

namespace SmartNet
{
    public class ConnectionList
    {
        private readonly List<Connection> connections = new List<Connection>(32);

        public ConnectionList()
        {
        }

        public ConnectionList(Connection connection)
        {
            Add(connection);
        }

        public ConnectionList(IEnumerable<Connection> connections)
        {
            foreach (var connection in connections)
            {
                Add(connection);
            }
        }

        public int Count => connections.Count;
        public Connection this[int index] => connections[index];

        public bool Add(Connection connection)
        {
            var id = connection.ConnectionId;

            if (Contains(id))
            {
                return false;
            }

            while (connections.Count <= id)
            {
                connections.Add(null);
            }

            connections[id] = connection;

            return true;
        }

        public bool Remove(int connectionId)
        {
            if (!Contains(connectionId))
            {
                return false;
            }

            connections[connectionId] = null;
            return true;
        }

        public bool Remove(Connection connection)
        {
            return Remove(connection.ConnectionId);
        }

        public Connection Get(int connectionId)
        {
            return !Contains(connectionId) ? null : connections[connectionId];
        }

        public bool Contains(Connection connection)
        {
            return Contains(connection.ConnectionId);
        }

        public bool Contains(int connectionId)
        {
            if (connectionId >= connections.Count)
            {
                return false;
            }

            return connections[connectionId] != null;
        }
    }
}