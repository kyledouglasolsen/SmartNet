﻿using System;
using MonoBehaviour = UnityEngine.MonoBehaviour;

namespace SmartNet
{
    public class DefaultNetVisibility : MonoBehaviour, INetVisibility
    {
        private SmartNetIdentity myIdentity;
        private Action<SmartNetIdentity> onAddKnownIdentity, onRemoveKnownIdentity;

        public Connection Owner { get; private set; }

        public void OnPreStart()
        {
        }

        public void OnStartServer()
        {
            myIdentity = GetComponent<SmartNetIdentity>();

            Owner = myIdentity.Owner;

            onAddKnownIdentity = onAddKnownIdentity ?? OnAddKnownIdentity;
            onRemoveKnownIdentity = onRemoveKnownIdentity ?? OnRemoveKnownIdentity;

            Owner.KnownIdentities.AddEvent -= onAddKnownIdentity;
            Owner.KnownIdentities.AddEvent += onAddKnownIdentity;

            Owner.KnownIdentities.RemoveEvent -= onRemoveKnownIdentity;
            Owner.KnownIdentities.RemoveEvent += onRemoveKnownIdentity;

            foreach (var identity in Owner.KnownIdentities)
            {
                Owner.AddVisible(identity);
            }
        }

        public void OnStopServer()
        {
            if (Owner != null)
            {
                Owner.KnownIdentities.AddEvent -= onAddKnownIdentity;
                Owner.KnownIdentities.RemoveEvent -= onRemoveKnownIdentity;
            }
        }

        public void OnStartOwner()
        {
        }

        public void OnStopOwner()
        {
        }

        public void OnStartClient()
        {
        }

        public void OnStopClient()
        {
        }

        public void OnServerOwnerRemoved(Connection owner)
        {
        }

        public void OnServerOwnerAdded(Connection owner)
        {
        }

        private void OnAddKnownIdentity(SmartNetIdentity identity)
        {
            Owner.AddVisible(identity);
        }

        private void OnRemoveKnownIdentity(SmartNetIdentity identity)
        {
            Owner.RemoveVisible(identity);
        }
    }
}