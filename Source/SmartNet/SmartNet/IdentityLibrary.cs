﻿using System;
using Debug = UnityEngine.Debug;
using ScriptableObject = UnityEngine.ScriptableObject;
using Vector3 = UnityEngine.Vector3;
using Quaternion = UnityEngine.Quaternion;
using System.Collections.Generic;

namespace SmartNet
{
    public class IdentityLibrary : ScriptableObject
    {
        public static IdentityLibrary Ins { get; private set; }
        public static event Action<SmartNetIdentity> PlayerSpawnEvent = delegate { };

        public SmartNetIdentity Player;
        public List<SmartNetIdentity> Library = new List<SmartNetIdentity>();

        private void OnEnable()
        {
            Ins = this;
        }

        public static bool Exists()
        {
            return Ins != null;
        }

        public static bool Add(SmartNetIdentity identity)
        {
            if (!Ins.Library.Contains(identity))
            {
                Ins.Library.Add(identity);
                return true;
            }

            return false;
        }

        public static bool Remove(SmartNetIdentity identity)
        {
            return Ins.Library.Remove(identity);
        }

        public static bool Contains(SmartNetIdentity identity)
        {
            return Ins.Library.Contains(identity);
        }

        public static bool Contains(uint assetId)
        {
            for (var i = 0; i < Ins.Library.Count; ++i)
            {
                if (Ins.Library[i]?.AssetId == assetId)
                {
                    return true;
                }
            }

            return false;
        }

        public static SmartNetIdentity Get(uint assetId)
        {
            for (var i = 0; i < Ins.Library.Count; ++i)
            {
                if (Ins.Library[i]?.AssetId == assetId)
                {
                    return Ins.Library[i];
                }
            }

            return null;
        }

        public static void RemoveNullReferences()
        {
            Ins?.Library?.RemoveAll(x => x == null);
        }

        public static SmartNetIdentity SpawnPlayer(Connection owner, Vector3 position, Quaternion rotation, KnownIdentities knownIdentities, NetworkId networkId, bool isServer)
        {
            return Spawn(owner, Ins.Player, position, rotation, knownIdentities, networkId, isServer);
        }

        public static SmartNetIdentity Spawn(Connection owner, uint assetId, Vector3 position, Quaternion rotation, KnownIdentities knownIdentities, NetworkId networkId, bool isServer)
        {
            return Spawn(owner, Get(assetId), position, rotation, knownIdentities, networkId, isServer);
        }

        public static SmartNetIdentity Spawn(Connection owner, SmartNetIdentity prefab, Vector3 position, Quaternion rotation, KnownIdentities knownIdentities, NetworkId networkId, bool isServer)
        {
            if (prefab == null)
            {
                return null;
            }

            var instance = Instantiate(prefab, position, rotation);
            SpawnInternal(owner, instance, knownIdentities, networkId, isServer);

            if (Ins.Player == prefab)
            {
                owner?.SetPlayer(instance);
                PlayerSpawnEvent(instance);
            }

            return instance;
        }

        private static void SpawnInternal(Connection owner, SmartNetIdentity identity, KnownIdentities knownIdentities, NetworkId networkId, bool isServer)
        {
            identity.SetOwnerInternal(owner);
            identity.SetNetworkId(networkId == NetworkId.Zero ? SmartNetIdentity.GetNextNetworkId() : networkId);
            identity.SetIsServer(isServer);
            identity.SetKnownIdentities(knownIdentities);

            identity.OnPreStart();

            if (isServer)
            {
                identity.OnStartServer();
            }

            if (identity.IsOwner)
            {
                identity.OnStartOwner();
            }

            if (!isServer && !identity.IsOwner)
            {
                identity.OnStartClient();
            }

            knownIdentities.Add(identity);
        }

        public static uint GetNextPrefabId()
        {
            for (var nextId = 0u; nextId < uint.MaxValue; ++nextId)
            {
                if (!Contains(nextId))
                {
                    return nextId;
                }
            }

            Debug.LogError($"Exceeded max Prefab count of {uint.MaxValue}. Critical failure!");
            return 0u;
        }
    }
}