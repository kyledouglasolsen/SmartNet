﻿namespace SmartNet
{
    public interface INetInitialize
    {
        void OnPreStart();

        void OnStartServer();
        void OnStopServer();

        void OnStartOwner();
        void OnStopOwner();

        void OnStartClient();
        void OnStopClient();

        void OnServerOwnerRemoved(Connection owner);
        void OnServerOwnerAdded(Connection owner);
    }
}