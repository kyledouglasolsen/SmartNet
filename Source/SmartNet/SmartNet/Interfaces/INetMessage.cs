﻿namespace SmartNet
{
    public interface INetMessage
    {
        void OnSerialize(Writer writer);
        void OnDeserialize(Reader reader);
    }
}