﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SmartNet
{
    public class KnownIdentities : IEnumerable<SmartNetIdentity>
    {
        private readonly HashSet<uint> knownIds = new HashSet<uint>();
        private SmartNetIdentity[] array = new SmartNetIdentity[128];
        private uint length;

        public event Action<SmartNetIdentity> AddEvent = delegate { };
        public event Action<SmartNetIdentity> RemoveEvent = delegate { };

        public bool Add(SmartNetIdentity identity)
        {
            var networkIdValue = identity.NetworkId.Value;

            if (!knownIds.Add(networkIdValue))
            {
                return false;
            }

            CheckExpandArray(networkIdValue + 1);
            array[networkIdValue] = identity;

            if (networkIdValue >= length)
            {
                length = networkIdValue + 1;
            }

            AddEvent(identity);

            return true;
        }

        public bool Remove(NetworkId networkId)
        {
            var networkIdValue = networkId.Value;

            if (!knownIds.Remove(networkIdValue))
            {
                return false;
            }

            var identity = array[networkIdValue];
            array[networkIdValue] = null;

            if (networkIdValue == length)
            {
                RecalculateLength();
            }

            RemoveEvent(identity);

            return true;
        }

        public SmartNetIdentity Get(NetworkId networkId)
        {
            if (networkId.Value >= array.Length)
            {
                return null;
            }

            return array[networkId.Value];
        }

        public bool Contains(NetworkId networkId)
        {
            return knownIds.Contains(networkId.Value);
        }

        private void CheckExpandArray(uint netId)
        {
            if (netId >= array.Length)
            {
                var newArray = new SmartNetIdentity[netId * 4];

                for (var i = 0; i < array.Length; ++i)
                {
                    newArray[i] = array[i];
                }

                array = newArray;
            }
        }

        private void RecalculateLength()
        {
            for (var i = array.LongLength - 1; i >= 0L; --i)
            {
                if (array[i] != null)
                {
                    length = (uint)i;
                    return;
                }
            }

            length = 0u;
        }

        public IEnumerator<SmartNetIdentity> GetEnumerator()
        {
            for (var i = 0u; i < length; ++i)
            {
                if (array[i] != null)
                {
                    yield return array[i];
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}