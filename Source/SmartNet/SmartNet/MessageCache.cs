﻿namespace SmartNet
{
    public static class MessageCache<T> where T : INetMessage, new()
    {
        private static readonly PooledStack<T> Pool = new PooledStack<T>(() => new T(), 32);

        public static T Pop()
        {
            return Pool.Pop();
        }

        public static void Push(T message)
        {
            Pool.Push(message);
        }
    }
}