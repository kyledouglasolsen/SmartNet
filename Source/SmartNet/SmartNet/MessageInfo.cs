﻿namespace SmartNet
{
    public class MessageInfo
    {
        public uint Type;
        public Connection Connection;
        public Reader Reader;
        public uint MessageSize;
        public int ChannelId;
        public bool IsServer;

        public MessageInfo()
        {
        }

        public MessageInfo(uint type, Connection connection, Reader reader, uint messageSize, int channelId, KnownIdentities knownIdentities, ConnectionList connections, bool isServer)
        {
            Initialize(type, connection, reader, messageSize, channelId, isServer);
        }

        public void Initialize(uint type, Connection connection, Reader reader, uint messageSize, int channelId, bool isServer)
        {
            Type = type;
            Connection = connection;
            Reader = reader;
            MessageSize = messageSize;
            ChannelId = channelId;
            IsServer = isServer;
        }

        public T ReadMessage<T>() where T : INetMessage, new()
        {
            var message = MessageCache<T>.Pop();

            try
            {
                message.OnDeserialize(Reader);
            }
            finally
            {
                MessageCache<T>.Push(message);
            }

            return message;
        }

        public void ReadMessage<T>(T msg) where T : INetMessage
        {
            msg.OnDeserialize(Reader);
        }
    }
}