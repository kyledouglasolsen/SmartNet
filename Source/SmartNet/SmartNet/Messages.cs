﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartNet.Messages
{
    public abstract class MessageTypes
    {
        protected static MessageTypes Instance = new DefaultMessageTypes();

        public static void SetMessageType(MessageTypes messageTypes)
        {
            Instance = messageTypes;
        }

        public static uint Count => Instance.MessageCount;
        public static Type[] Types => Instance.AllTypes;

        public static Type GetType(uint typeId)
        {
            return Instance.GetTypeFromId(typeId);
        }

        public static uint GetTypeId(Type type)
        {
            return Instance.GetTypeIdFromType(type);
        }

        public static uint GetTypeId<T>() where T : INetMessage
        {
            return Instance.GetTypeIdGeneric<T>();
        }

        public abstract uint MessageCount { get; }
        public abstract Type[] AllTypes { get; }

        public abstract Type GetTypeFromId(uint typeId);
        public abstract uint GetTypeIdFromType(Type type);
        public abstract uint GetTypeIdGeneric<T>() where T : INetMessage;
    }

    public class DefaultMessageTypes : MessageTypes
    {
        public DefaultMessageTypes()
        {
            const string builtinMessageNamespace = "SmartNet.Messages";

            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(x => typeof(INetMessage).IsAssignableFrom(x) && !x.IsInterface && x.Namespace == builtinMessageNamespace).OrderBy(x => x.FullName)
                .ToArray();

            for (var i = 0; i < types.Length; ++i)
            {
                var type = types[i];
                var id = (uint)(i + 1);
                TypeToId[type] = id;
                IdToType[id] = type;
            }

            if (types.Length < Default.BuiltinTypesMinLength)
            {
                Array.Resize(ref types, Default.BuiltinTypesMinLength);
            }

            MessageCount = (uint)types.LongLength;
            AllTypes = types;
        }

        private static readonly Dictionary<Type, uint> TypeToId = new Dictionary<Type, uint>();
        private static readonly Dictionary<uint, Type> IdToType = new Dictionary<uint, Type>();

        public override uint MessageCount { get; }
        public override Type[] AllTypes { get; }

        public override Type GetTypeFromId(uint typeId)
        {
            return IdToType.TryGetValue(typeId, out Type type) ? type : null;
        }

        public override uint GetTypeIdFromType(Type type)
        {
            return TypeToId.TryGetValue(type, out uint messageType) ? messageType : 0u;
        }

        public override uint GetTypeIdGeneric<T>()
        {
            return GetTypeIdFromType(typeof(T));
        }
    }
}