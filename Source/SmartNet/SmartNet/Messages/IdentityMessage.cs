﻿using SmartNet.Profiler;
using Debug = UnityEngine.Debug;

namespace SmartNet.Messages
{
    public class IdentityMessage : INetMessage
    {
        public NetworkId NetworkId;
        public uint MessageType;
        public INetMessage InnerMessage;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
            writer.WriteCompressed(MessageType);
            InnerMessage.OnSerialize(writer);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
            MessageType = reader.ReadUIntCompressed();
        }

        public void Initialize(NetworkId networkId, uint messageType, INetMessage message)
        {
            NetworkId = networkId;
            MessageType = messageType;
            InnerMessage = message;
        }

        public static void HandleMessage(MessageInfo info)
        {
            var message = info.ReadMessage<IdentityMessage>();
            var identity = info.Connection.KnownIdentities.Get(message.NetworkId);
            var messageType = MessageTypes.GetType(message.MessageType);

            Statistics.Add<IdentityMessage>(NetworkDirection.Inbound, messageType.Name, info.MessageSize);
            
            if (identity == null)
            {
                Debug.LogWarning($"Received IdentityMessage<{messageType?.GetType().Name}> for unknown NetworkId {message.NetworkId}");
                return;
            }

            var cachedMessage = identity.GetCachedMessage(message.MessageType);
            cachedMessage.OnDeserialize(info.Reader);

            if (identity.Owner == info.Connection)
            {
                identity.InvokeHandler(message.MessageType, cachedMessage);
            }
        }
    }
}