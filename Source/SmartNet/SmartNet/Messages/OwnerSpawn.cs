﻿using Vector3 = UnityEngine.Vector3;
using Quaternion = UnityEngine.Quaternion;

namespace SmartNet.Messages
{
    public class OwnerSpawn : INetMessage
    {
        public uint AssetId;
        public NetworkId NetworkId;
        public Vector3 Position;
        public Quaternion Rotation;

        public OwnerSpawn()
        {
        }

        public OwnerSpawn(SmartNetIdentity identity)
        {
            Initialize(identity);
        }

        public void Initialize(SmartNetIdentity identity)
        {
            AssetId = identity.AssetId;
            NetworkId = identity.NetworkId;
            Position = identity.transform.position;
            Rotation = identity.transform.rotation;
        }

        public void OnSerialize(Writer writer)
        {
            writer.WriteCompressed(AssetId);
            writer.Write(NetworkId);
            writer.Write(Position);
            writer.WriteCompressed(Rotation);
        }

        public void OnDeserialize(Reader reader)
        {
            AssetId = reader.ReadUIntCompressed();
            NetworkId = reader.ReadNetworkId();
            Position = reader.ReadVector3();
            Rotation = reader.ReadQuaternionCompressed();
        }

        public static void HandleMessage(GenericMessageInfo<OwnerSpawn> info)
        {
            IdentityLibrary.Spawn(info.Connection, info.Message.AssetId, info.Message.Position, info.Message.Rotation, info.Connection.KnownIdentities, info.Message.NetworkId, info.IsServer);
        }
    }
}