﻿namespace SmartNet.Messages
{
    public class RemoveOwner : INetMessage
    {
        public NetworkId NetworkId;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
        }

        public static void HandleMessage(GenericMessageInfo<RemoveOwner> info)
        {
            var id = info.Message.NetworkId;
            var identity = info.Connection.KnownIdentities.Get(id);
            identity.SetOwnerInternal(null);

            identity.OnStopOwner();

            if (!identity.IsServer)
            {
                identity.OnStartClient();
            }
        }
    }
}