﻿using Object = UnityEngine.Object;

namespace SmartNet.Messages
{
    public class WorldDestroy : INetMessage
    {
        public NetworkId NetworkId;

        public void OnSerialize(Writer writer)
        {
            writer.Write(NetworkId);
        }

        public void OnDeserialize(Reader reader)
        {
            NetworkId = reader.ReadNetworkId();
        }

        public static void HandleMessage(GenericMessageInfo<WorldDestroy> info)
        {
            var identity = info.Connection.KnownIdentities.Get(info.Message.NetworkId);

            if (identity != null)
            {
                if (identity.IsServer)
                {
                    identity.OnStopServer();
                }

                if (identity.IsOwner)
                {
                    identity.OnStopOwner();
                }

                if (!identity.IsServer && !identity.IsOwner)
                {
                    identity.OnStopClient();
                }

                info.Connection.KnownIdentities.Remove(info.Message.NetworkId);
                Object.Destroy(identity.gameObject);
            }
        }
    }
}