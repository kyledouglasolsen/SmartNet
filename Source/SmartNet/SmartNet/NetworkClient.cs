﻿using System;

namespace SmartNet
{
    public class NetworkClient
    {
        public static Client Local { get; private set; }

        private static Client clientInstance;

        private static Client Client
        {
            get
            {
                if (clientInstance == null)
                {
                    clientInstance = new Client();
                    Local = clientInstance;
                }

                return clientInstance;
            }
        }

        private NetworkClient()
        {
        }

        public static string Ip => Client.Ip;
        public static int Port => Client.Port;
        public static Connection Connection => Client.Connection;
        public static ConnectState State => Client.State;

        public static event Action ConnectEvent
        {
            add => Client.ConnectEvent += value;
            remove => Client.ConnectEvent -= value;
        }

        public static event Action DisconnectEvent
        {
            add => Client.DisconnectEvent += value;
            remove => Client.DisconnectEvent -= value;
        }

        public static void Configure(ServerSettings serverSettings)
        {
            Client.Configure(serverSettings);
        }

        public static void Connect(string ip, int port)
        {
            Client.Connect(ip, port);
        }

        public static void Disconnect()
        {
            Client.Disconnect();
        }

        public static void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            Client.RegisterHandler(handler);
        }

        public static void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            Client.RegisterHandler(messageType, handler);
        }

        public static void UnregisterHandler<T>() where T : INetMessage
        {
            Client.UnregisterHandler<T>();
        }

        public static void UnregisterHandler(uint messageType)
        {
            Client.UnregisterHandler(messageType);
        }

        public static bool HasHandler<T>() where T : INetMessage
        {
            return Client.HasHandler<T>();
        }

        public static bool HasHandler(uint messageType)
        {
            return Client.HasHandler(messageType);
        }

        public static bool Send<T>(T message, int channelId) where T : INetMessage
        {
            return Client.Send(message, channelId);
        }

        public static bool Send(INetMessage message, int channelId)
        {
            return Client.Send(message, channelId);
        }

        public static bool Send(uint messageType, INetMessage message, int channelId)
        {
            return Client.Send(messageType, message, channelId);
        }

        public static bool Send(Writer writer, int channelId)
        {
            return Client.Send(writer, channelId);
        }

        public static bool Send(byte[] bytes, int count, int channelId)
        {
            return Client.Send(bytes, count, channelId);
        }
    }
}