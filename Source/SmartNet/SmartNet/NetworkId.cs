﻿using SerializeField = UnityEngine.SerializeField;
using System;

namespace SmartNet
{
    [Serializable]
    public struct NetworkId
    {
        [SerializeField] private readonly uint value;

        public NetworkId(uint value)
        {
            this.value = value;
        }

        public uint Value => value;

        public bool IsEmpty()
        {
            return value == 0;
        }

        public override int GetHashCode()
        {
            return (int)value;
        }

        public override bool Equals(object obj)
        {
            return obj is NetworkId && this == (NetworkId)obj;
        }

        public static bool operator ==(NetworkId c1, NetworkId c2)
        {
            return c1.value == c2.value;
        }

        public static bool operator !=(NetworkId c1, NetworkId c2)
        {
            return c1.value != c2.value;
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public static NetworkId Zero = new NetworkId(0u);
    }
}