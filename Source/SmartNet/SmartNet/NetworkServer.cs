﻿using System;

namespace SmartNet
{
    public class NetworkServer
    {
        private static Server serverInstance;
        private static Server Server => serverInstance ?? (serverInstance = new Server());

        private NetworkServer()
        {
        }

        public static int HostId => Server.HostId;
        public static int Port => Server.Port;
        public static KnownIdentities KnownIdentities => Server.KnownIdentities;

        public static event Action StartEvent
        {
            add => Server.StartEvent += value;
            remove => Server.StartEvent -= value;
        }

        public static event Action StopEvent
        {
            add => Server.StopEvent += value;
            remove => Server.StopEvent -= value;
        }

        public static event Action<Connection> NewConnectionEvent
        {
            add => Server.NewConnectionEvent += value;
            remove => Server.NewConnectionEvent -= value;
        }

        public static event Action<Connection> ConnectionDisconnectEvent
        {
            add => Server.ConnectionDisconnectEvent += value;
            remove => Server.ConnectionDisconnectEvent -= value;
        }

        public static void Configure(ServerSettings settings)
        {
            Server.Configure(settings);
        }

        public static void Start(string ip, int port)
        {
            Server.Start(ip, port);
        }

        public static void Stop()
        {
            Server.Stop();
        }

        public static bool SendToAll<T>(T message, int channelId) where T : INetMessage
        {
            return Server.SendToAll(message, channelId);
        }

        public static bool SendToAll(uint messageType, INetMessage message, int channelId)
        {
            return Server.SendToAll(messageType, message, channelId);
        }

        public static bool SendToObservers<T>(Connection connection, T message, int channelId) where T : INetMessage
        {
            return Server.SendToObservers(connection, message, channelId);
        }

        public static bool SendToObservers(Connection connection, uint messageType, INetMessage message, int channelId)
        {
            return Server.SendToObservers(connection, messageType, message, channelId);
        }

        public static void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            Server.RegisterHandler(handler);
        }

        public static void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            Server.RegisterHandler(messageType, handler);
        }

        public static void UnregisterHandler<T>() where T : INetMessage
        {
            Server.UnregisterHandler<T>();
        }

        public static void UnregisterHandler(uint messageType)
        {
            Server.UnregisterHandler(messageType);
        }

        public static bool HasHandler<T>() where T : INetMessage
        {
            return Server.HasHandler<T>();
        }

        public static bool HasHandler(uint messageType)
        {
            return Server.HasHandler(messageType);
        }
    }
}