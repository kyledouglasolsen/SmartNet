﻿using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using SmartNet.Messages;

namespace SmartNet
{
    public static class NetworkTime
    {
        private const int BroadcastDelayMilliseconds = 1000;
        private static int milliseconds, nextServerBroadcastTime, lastReceivedLocalTime, lastReceivedServerTime;
        private static readonly UpdateTime UpdateTime = new UpdateTime();

        public static int Milliseconds
        {
            get => milliseconds;
            private set
            {
                milliseconds = value;
                Seconds = milliseconds / 1000f;
            }
        }

        public static float Seconds { get; private set; }

        static NetworkTime()
        {
            Milliseconds = NetworkTransport.GetNetworkTimestamp();
            nextServerBroadcastTime = Milliseconds + BroadcastDelayMilliseconds;
        }

        public static void Update(bool isServer)
        {
            var now = NetworkTransport.GetNetworkTimestamp();

            if (isServer)
            {
                Milliseconds = now;

                if (Milliseconds >= nextServerBroadcastTime)
                {
                    UpdateTime.Time = Milliseconds;

                    for (var i = 0; i < Server.Servers.Count; ++i)
                    {
                        Server.Servers[i].SendToAll(UpdateTime, 0);
                    }

                    nextServerBroadcastTime = Milliseconds + BroadcastDelayMilliseconds;
                }
            }
            else
            {
                Milliseconds = lastReceivedServerTime + now - lastReceivedLocalTime;
            }
        }

        public static void ReceiveTimeFromServer(GenericMessageInfo<UpdateTime> info)
        {
            var serverTime = info.Message.Time;
            var delayMs = NetworkTransport.GetRemoteDelayTimeMS(info.Connection.HostId, info.Connection.ConnectionId, serverTime, out byte error);
            var newTime = serverTime + delayMs;
            Milliseconds = newTime;
            lastReceivedLocalTime = NetworkTransport.GetNetworkTimestamp();
            lastReceivedServerTime = newTime;
        }
    }
}