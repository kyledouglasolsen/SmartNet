﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SmartNet.Profiler
{
    public class BarGroup : MonoBehaviour
    {
        private readonly List<Image> images = new List<Image>();

        public float Height { get; private set; }

        public void Add(Image image)
        {
            images.Add(image);
            image.rectTransform.anchoredPosition = new Vector2(0f, Height);
            Height += image.rectTransform.sizeDelta.y;
        }

        public void Recycle(PooledStack<Image> pool)
        {
            for (var i = 0; i < images.Count; ++i)
            {
                pool.Push(images[i]);
                images[i].gameObject.SetActive(false);
            }

            images.Clear();

            gameObject.SetActive(false);
            Height = 0f;
        }
    }
}