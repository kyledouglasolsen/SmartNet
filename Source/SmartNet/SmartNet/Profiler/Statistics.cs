﻿using System;
using System.Collections.Generic;
using SmartNet.Messages;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SmartNet.Profiler
{
    public static class Statistics
    {
        internal static readonly BandwidthDetails Bandwidth = new BandwidthDetails();
        internal static readonly Dictionary<uint, NetworkOperationDetails> MessageTypeOperationDetails = new Dictionary<uint, NetworkOperationDetails>();
        private static float lastTickTime;
        private static int lastTickId;
        private const float DelayBetweenTicks = 0.5f;

        public static event Action TickEvent = delegate { };

        public static void Tick(float newTime)
        {
            if (newTime - lastTickTime <= DelayBetweenTicks)
            {
                return;
            }

            lastTickTime = newTime;
            lastTickId = (int)lastTickTime % 20;

            Bandwidth.Tick(lastTickId);

            foreach (var details in MessageTypeOperationDetails.Values)
            {
                details.Tick(lastTickId);
            }

            TickEvent();
        }

        public static void Add<T>(NetworkDirection direction, uint size, uint amount = 1) where T : INetMessage
        {
            Add<T>(direction, typeof(T).Name, size, amount);
        }

        public static void Add<T>(NetworkDirection direction, string entryName, uint size, uint amount = 1u) where T : INetMessage
        {
            Add(MessageTypes.GetTypeId<T>(), direction, entryName, size, amount);
        }

        public static void Add(uint messageType, NetworkDirection direction, uint size, uint amount = 1u)
        {
            Add(messageType, direction, MessageTypes.GetType(messageType).Name, size, amount);
        }

        public static void Add(uint messageType, NetworkDirection direction, string entryName, uint size, uint amount = 1u)
        {
            NetworkOperationDetails details;

            if (!MessageTypeOperationDetails.TryGetValue(messageType, out details))
            {
                details = new NetworkOperationDetails(messageType);
                MessageTypeOperationDetails[messageType] = details;
            }

            details.Add(direction, entryName, size, amount);
        }

        public static void Set<T>(NetworkDirection direction, string entryName, uint size, uint amount = 1u) where T : INetMessage
        {
            Set(MessageTypes.GetTypeId<T>(), direction, entryName, size, amount);
        }

        public static void Set(uint messageType, NetworkDirection direction, string entryName, uint size, uint amount = 1u)
        {
            NetworkOperationDetails details;

            if (!MessageTypeOperationDetails.TryGetValue(messageType, out details))
            {
                details = new NetworkOperationDetails(messageType);
                MessageTypeOperationDetails[messageType] = details;
            }

            details.Set(direction, entryName, size, amount);
        }

        public static void AddToBandwidth(NetworkDirection direction, uint size)
        {
            Bandwidth.Add(direction, size);
        }

        internal class BandwidthDetails
        {
            private readonly NetworkStatsSequence inboundSequence = new NetworkStatsSequence();
            private readonly NetworkStatsSequence outboundSequence = new NetworkStatsSequence();

            public void Tick(int tickId)
            {
                inboundSequence.Tick(tickId);
                outboundSequence.Tick(tickId);
            }

            public void Add(NetworkDirection direction, uint size)
            {
                if (direction != NetworkDirection.Inbound)
                {
                    if (direction != NetworkDirection.Outbound)
                    {
                        return;
                    }

                    outboundSequence.Add(lastTickId, size, 1);
                }
                else
                {
                    inboundSequence.Add(lastTickId, size, 1);
                }
            }

            public uint InBytesPerSecond()
            {
                return (uint)(inboundSequence.GetBytesOverFive(lastTickId) / DelayBetweenTicks);
            }

            public uint OutBytesPerSecond()
            {
                return (uint)(outboundSequence.GetBytesOverFive(lastTickId) / DelayBetweenTicks);
            }
        }

        internal class NetworkOperationDetails
        {
            public readonly Dictionary<string, NetworkOperationEntryDetails> Entries = new Dictionary<string, NetworkOperationEntryDetails>();

            public uint MessageType;
            public uint MessageCountIn, LastMessageCountIn;
            public uint MessageCountOut, LastMessageCountOut;
            public uint BytesIn, LastBytesIn;
            public uint BytesOut, LastBytesOut;

            public NetworkOperationDetails(uint messageType)
            {
                MessageType = messageType;
            }

            public void Tick(int tickId)
            {
                LastMessageCountIn = MessageCountIn;
                LastMessageCountOut = MessageCountOut;
                LastBytesIn = BytesIn;
                LastBytesOut = BytesOut;

                foreach (var operationEntryDetails in Entries.Values)
                {
                    operationEntryDetails.Tick(tickId);
                }

                MessageCountIn = 0;
                MessageCountOut = 0;
                BytesIn = 0;
                BytesOut = 0;
            }

            public void Clear()
            {
                foreach (var operationEntryDetails in Entries.Values)
                {
                    operationEntryDetails.Clear();
                }

                MessageCountIn = 0;
                MessageCountOut = 0;
                LastMessageCountIn = 0;
                LastMessageCountOut = 0;
                BytesIn = 0;
                BytesOut = 0;
                LastBytesIn = 0;
                LastBytesOut = 0;
            }

            public void Set(NetworkDirection direction, string entryName, uint size, uint amount)
            {
                NetworkOperationEntryDetails details;

                if (!Entries.TryGetValue(entryName, out details))
                {
                    details = new NetworkOperationEntryDetails {EntryName = entryName};
                    Entries[entryName] = details;
                }

                details.Add(direction, size, amount);

                if (direction != NetworkDirection.Inbound)
                {
                    if (direction != NetworkDirection.Outbound)
                    {
                        return;
                    }

                    MessageCountOut = amount;
                    BytesOut = size;
                }
                else
                {
                    MessageCountIn = amount;
                    BytesIn = size;
                }
            }

            public void Add(NetworkDirection direction, string entryName, uint size, uint amount)
            {
                NetworkOperationEntryDetails details;

                if (!Entries.TryGetValue(entryName, out details))
                {
                    details = new NetworkOperationEntryDetails {EntryName = entryName};
                    Entries[entryName] = details;
                }

                details.Add(direction, size, amount);

                if (direction != NetworkDirection.Inbound)
                {
                    if (direction != NetworkDirection.Outbound)
                    {
                        return;
                    }

                    MessageCountOut += amount;
                    BytesOut += size;
                }
                else
                {
                    MessageCountIn += amount;
                    BytesIn += size;
                }
            }
        }

        internal class NetworkOperationEntryDetails
        {
            public readonly NetworkStatsSequence InboundSequence = new NetworkStatsSequence();
            public readonly NetworkStatsSequence OutboundSequence = new NetworkStatsSequence();
            public readonly Color Color;
            public string EntryName;
            public uint InboundMessageTotal, InboundBytesTotal;
            public uint OutboundMessageTotal, OutboundBytesTotal;

            public NetworkOperationEntryDetails()
            {
                Color = new Color(Random.value, Random.value, Random.value, 1f);
            }

            public void Tick(int tickId)
            {
                InboundSequence.Tick(tickId);
                OutboundSequence.Tick(tickId);
            }

            public void Clear()
            {
                InboundMessageTotal = 0;
                OutboundMessageTotal = 0;
            }

            public void Add(NetworkDirection direction, uint size, uint amount)
            {
                if (direction != NetworkDirection.Inbound)
                {
                    if (direction != NetworkDirection.Outbound)
                    {
                        return;
                    }

                    OutboundMessageTotal += amount;
                    OutboundBytesTotal += size;
                    OutboundSequence.Add(lastTickId, size, amount);
                }
                else
                {
                    InboundMessageTotal += amount;
                    InboundBytesTotal += size;
                    InboundSequence.Add(lastTickId, size, amount);
                }
            }
        }

        internal class NetworkStatsSequence
        {
            private readonly uint[] messagesPerTick = new uint[20];
            private readonly uint[] bytesPerTick = new uint[20];
            public uint MessageTotal;
            public uint BytesTotal;

            public void Tick(int tick)
            {
                MessageTotal -= messagesPerTick[tick];
                BytesTotal -= bytesPerTick[tick];
                messagesPerTick[tick] = 0;
                bytesPerTick[tick] = 0;
            }

            public void Add(int tick, uint size, uint amount)
            {
                messagesPerTick[tick] += amount;
                bytesPerTick[tick] += size;
                MessageTotal += amount;
                BytesTotal += size;
            }

            public uint GetMessageCountOverFive(int tick)
            {
                var num = 0u;

                for (var index = 0; index < 5; ++index)
                {
                    num += messagesPerTick[(tick - index + 20) % 20];
                }

                return num / 5;
            }

            public uint GetMessageCountOverTen(int tick)
            {
                var num = 0u;

                for (var index = 0; index < 10; ++index)
                {
                    num += messagesPerTick[(tick - index + 20) % 20];
                }

                return num / 10;
            }

            public uint GetBytesOverFive(int tick)
            {
                var num = 0u;

                for (var index = 0; index < 5; ++index)
                {
                    num += bytesPerTick[(tick - index + 20) % 20];
                }

                return num / 5;
            }

            public uint GetBytesOverTen(int tick)
            {
                var num = 0u;

                for (var index = 0; index < 10; ++index)
                {
                    num += bytesPerTick[(tick - index + 20) % 20];
                }

                return num / 10;
            }
        }
    }

    public enum NetworkDirection
    {
        Outbound,
        Inbound
    }
}