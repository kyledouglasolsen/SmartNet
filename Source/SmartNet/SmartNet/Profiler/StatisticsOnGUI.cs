﻿using UnityEngine;

namespace SmartNet.Profiler
{
    public class StatisticsOnGUI : MonoBehaviour
    {
        private void OnGUI()
        {
            var tick = (int)Time.time;

            GUILayout.Label($"In {Statistics.Bandwidth.InBytesPerSecond()}B/s");
            GUILayout.Label($"Out {Statistics.Bandwidth.OutBytesPerSecond()}B/s");

            foreach (var kvp in Statistics.MessageTypeOperationDetails)
            {
                foreach (var entry in kvp.Value.Entries)
                {
                    var details = entry.Value;
                    var inbound = details.InboundSequence;
                    var outbound = details.OutboundSequence;

                    GUILayout.Label($"{entry.Key}");
                    GUILayout.Label($" In: {inbound.GetMessageCountOverFive(tick)} ({inbound.GetBytesOverFive(tick)}B/s)");
                    GUILayout.Label($" Out: {outbound.GetMessageCountOverFive(tick)} ({outbound.GetBytesOverFive(tick)}B/s)");
                    GUILayout.Label($" In Total: {details.InboundMessageTotal} ({details.InboundBytesTotal} bytes)");
                    GUILayout.Label($" Out Total: {details.OutboundMessageTotal} ({details.OutboundBytesTotal} bytes)");
                }
            }
        }
    }
}