﻿using System.Collections.Generic;
using SmartNet.Messages;
using UnityEngine;
using UnityEngine.UI;

namespace SmartNet.Profiler
{
    public class StatisticsUI : MonoBehaviour
    {
        public class BarComparer : IComparer<BarGroup>
        {
            public int Compare(BarGroup a, BarGroup b)
            {
                return b.Height.CompareTo(a.Height);
            }
        }

        private static Canvas canvas;
        private static RectTransform horizontalRectTransform;
        private static HorizontalLayoutGroup horizontalParent;
        private static readonly Queue<BarGroup> ActiveBarGroups = new Queue<BarGroup>();

        private static HorizontalLayoutGroup HorizontalParent
        {
            get
            {
                if (canvas == null)
                {
                    CreateCanvas();
                }

                return horizontalParent;
            }
        }

        private static Canvas Canvas
        {
            get
            {
                if (canvas == null)
                {
                    CreateCanvas();
                }

                return canvas;
            }
        }

        private const float BarWidth = 10f;
        private static int maxBars = 512;
        private static float scale = 1f;
        private static BarGroup tallestGroup;

        private static readonly PooledStack<Image> BarPool = new PooledStack<Image>(() =>
        {
            var go = new GameObject("Bar");
            var transform = go.AddComponent<RectTransform>();
            var image = transform.gameObject.AddComponent<Image>();
            image.rectTransform.pivot = new Vector2(1f, 0f);
            image.rectTransform.anchorMin = new Vector2(1f, 0f);
            image.rectTransform.anchorMax = new Vector2(1f, 0f);
            image.raycastTarget = false;
            return image;
        });

        private static readonly PooledStack<BarGroup> GroupPool = new PooledStack<BarGroup>(() =>
        {
            var go = new GameObject("Group");
            go.transform.SetParent(HorizontalParent.transform);
            var transform = go.AddComponent<RectTransform>();
            transform.anchoredPosition3D = new Vector3(0f, 0f, 0f);
            transform.pivot = new Vector2(1f, 0f);
            transform.sizeDelta = new Vector2(BarWidth, transform.sizeDelta.y);
            transform.localScale = new Vector3(1f, scale, 1f);
            var barGroup = go.AddComponent<BarGroup>();
            return barGroup;
        });

        private static void CreateCanvas()
        {
            canvas = new GameObject("Statistics UI").AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.pixelPerfect = false;
            canvas.sortingOrder = 0;
            canvas.additionalShaderChannels = AdditionalCanvasShaderChannels.None;

            var scaler = canvas.gameObject.AddComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            scaler.scaleFactor = 1f;
            scaler.referencePixelsPerUnit = 100;

            var horizontalGo = new GameObject("Horizontal Parent");
            horizontalGo.transform.SetParent(canvas.transform);
            horizontalRectTransform = horizontalGo.AddComponent<RectTransform>();
            horizontalRectTransform.anchorMin = new Vector2(0f, 0f);
            horizontalRectTransform.anchorMax = new Vector2(1f, 0.1f);
            horizontalRectTransform.pivot = new Vector2(0.5f, 0f);
            horizontalRectTransform.offsetMin = new Vector2(0f, 0f);
            horizontalRectTransform.offsetMax = new Vector2(1f, 1f);
            horizontalParent = horizontalGo.AddComponent<HorizontalLayoutGroup>();
            horizontalParent.childAlignment = TextAnchor.LowerRight;
            horizontalParent.childControlWidth = false;
            horizontalParent.childControlHeight = true;
            horizontalParent.childForceExpandWidth = false;
            horizontalParent.childForceExpandHeight = true;
            horizontalGo.AddComponent<Image>();
            horizontalGo.AddComponent<Mask>().showMaskGraphic = false;

            UpdateMaxBars();

            canvas.gameObject.AddComponent<StatisticsUI>();
        }

        public static void Show()
        {
            Canvas.gameObject.SetActive(true);
        }

        public static void Hide()
        {
            Canvas.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            Statistics.TickEvent += StatisticsOnTickEvent;
        }

        private void OnDisable()
        {
            Statistics.TickEvent -= StatisticsOnTickEvent;
        }

        private static void StatisticsOnTickEvent()
        {
            if (Statistics.MessageTypeOperationDetails.Count < 1)
            {
                return;
            }

            UpdateMaxBars();

            var tick = (int)Time.time;

            BarGroup group = null;

            while (ActiveBarGroups.Count > maxBars)
            {
                group = ActiveBarGroups.Dequeue();
                group.Recycle(BarPool);
            }

            if (group == null)
            {
                group = GroupPool.Pop();
            }

            group.gameObject.SetActive(true);
            group.transform.SetAsLastSibling();

            foreach (var details in Statistics.MessageTypeOperationDetails.Values)
            {
                var messageType = MessageTypes.GetType(details.MessageType);
                var count = details.Entries.Count;

                foreach (var entry in details.Entries)
                {
                    if (count > 1 && entry.Key == messageType.Name)
                    {
                        continue;
                    }

                    var bytes = entry.Value.InboundSequence.GetBytesOverFive(tick) + entry.Value.OutboundSequence.GetBytesOverFive(tick);

                    if (bytes <= 0)
                    {
                        continue;
                    }

                    var bar = BarPool.Pop();
                    bar.transform.SetSiblingIndex((int)details.MessageType);
                    bar.color = entry.Value.Color;
                    bar.transform.SetParent(group.transform);

                    bar.rectTransform.sizeDelta = new Vector2(BarWidth, bytes);
                    bar.transform.localScale = Vector3.one;
                    group.Add(bar);

                    bar.gameObject.SetActive(true);
                }
            }

            if (group.Height > 0f)
            {
                ActiveBarGroups.Enqueue(group);
            }
            else
            {
                group.Recycle(BarPool);
            }

            UpdateTallestGroup();
        }

        private static void UpdateScale()
        {
            if (tallestGroup == null)
            {
                scale = 1f;
                return;
            }

            if (tallestGroup.Height > 0f)
            {
                scale = horizontalRectTransform.rect.height / tallestGroup.Height;
            }
            else
            {
                scale = 1f;
            }

            foreach (var group in ActiveBarGroups)
            {
                group.transform.localScale = new Vector3(1f, scale, 1f);
            }
        }

        private static void UpdateMaxBars()
        {
            maxBars = Mathf.FloorToInt(horizontalRectTransform.rect.width / BarWidth);
        }

        private static void UpdateTallestGroup()
        {
            tallestGroup = null;

            foreach (var group in ActiveBarGroups)
            {
                if (tallestGroup == null || group.Height > tallestGroup.Height)
                {
                    tallestGroup = group;
                }
            }

            UpdateScale();
        }
    }
}