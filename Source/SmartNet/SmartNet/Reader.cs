﻿using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;
using Color = UnityEngine.Color;
using Color32 = UnityEngine.Color32;
using Quaternion = UnityEngine.Quaternion;
using Rect = UnityEngine.Rect;
using Plane = UnityEngine.Plane;
using Ray = UnityEngine.Ray;
using Matrix4x4 = UnityEngine.Matrix4x4;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace SmartNet
{
    public class Reader
    {
        private readonly Buffer buffer;
        private const int MaxStringLength = 1024 * 32;
        private const int InitialStringBufferSize = 1024;
        private byte[] stringReaderBuffer;
        private static Encoding encoding;

        public Reader()
        {
            buffer = new Buffer();
            Initialize();
        }

        public Reader(Writer writer)
        {
            buffer = new Buffer(writer.AsArray());
            Initialize();
        }

        public Reader(byte[] buffer)
        {
            this.buffer = new Buffer(buffer);
            Initialize();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Initialize()
        {
            stringReaderBuffer = new byte[InitialStringBufferSize];
            encoding = encoding ?? new UTF8Encoding();
        }

        public uint Position => buffer.Position;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] AsArray()
        {
            return buffer.AsArray();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> AsSegment()
        {
            return buffer.AsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetReadBytesAsSegment()
        {
            return buffer.GetReadBytesAsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetRemainingBytesAsSegment()
        {
            return buffer.GetRemainingBytesAsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SeekZero()
        {
            buffer.SeekZero();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void MovePosition(uint count)
        {
            buffer.MovePosition(count);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(byte[] bytes)
        {
            buffer.Replace(bytes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(Reader reader)
        {
            buffer.Replace(reader.buffer);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Replace(ArraySegment<byte> bytes)
        {
            buffer.Replace(bytes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte ReadByte()
        {
            return buffer.ReadByte();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sbyte ReadSByte()
        {
            return (sbyte)buffer.ReadByte();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public short ReadShort()
        {
            ushort value = 0;
            value |= buffer.ReadByte();
            value |= (ushort)(buffer.ReadByte() << 8);
            return (short)value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ushort ReadUShort()
        {
            ushort value = 0;
            value |= buffer.ReadByte();
            value |= (ushort)(buffer.ReadByte() << 8);
            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int ReadInt()
        {
            uint value = 0;
            value |= buffer.ReadByte();
            value |= (uint)(buffer.ReadByte() << 8);
            value |= (uint)(buffer.ReadByte() << 16);
            value |= (uint)(buffer.ReadByte() << 24);
            return (int)value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int ReadIntCompressed()
        {
            return Buffer.ZigZagDecode(ReadUIntCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public uint ReadUInt()
        {
            uint value = 0;
            value |= buffer.ReadByte();
            value |= (uint)(buffer.ReadByte() << 8);
            value |= (uint)(buffer.ReadByte() << 16);
            value |= (uint)(buffer.ReadByte() << 24);
            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public uint ReadUIntCompressed()
        {
            var a0 = ReadByte();
            if (a0 < 241)
            {
                return a0;
            }

            var a1 = ReadByte();
            if (a0 >= 241 && a0 <= 248)
            {
                return (uint)(240 + 256 * (a0 - 241) + a1);
            }

            var a2 = ReadByte();
            if (a0 == 249)
            {
                return (uint)(2288 + 256 * a1 + a2);
            }

            var a3 = ReadByte();
            if (a0 == 250)
            {
                return a1 + (((uint)a2) << 8) + (((uint)a3) << 16);
            }

            var a4 = ReadByte();
            if (a0 >= 251)
            {
                return a1 + (((uint)a2) << 8) + (((uint)a3) << 16) + (((uint)a4) << 24);
            }

            throw new IndexOutOfRangeException("ReadUIntCompressed() failure: " + a0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ReadLong()
        {
            ulong value = 0;

            ulong other = buffer.ReadByte();
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 8;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 16;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 24;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 32;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 40;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 48;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 56;
            value |= other;

            return (long)value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long ReadLongCompressed()
        {
            return Buffer.ZigZagDecode(ReadULongCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong ReadULong()
        {
            ulong value = 0;
            ulong other = buffer.ReadByte();
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 8;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 16;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 24;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 32;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 40;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 48;
            value |= other;

            other = ((ulong)buffer.ReadByte()) << 56;
            value |= other;
            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong ReadULongCompressed()
        {
            var a0 = ReadByte();
            if (a0 < 241)
            {
                return a0;
            }

            var a1 = ReadByte();
            if (a0 >= 241 && a0 <= 248)
            {
                return 240 + 256 * (a0 - ((ulong)241)) + a1;
            }

            var a2 = ReadByte();
            if (a0 == 249)
            {
                return 2288 + (((ulong)256) * a1) + a2;
            }

            var a3 = ReadByte();
            if (a0 == 250)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16);
            }

            var a4 = ReadByte();
            if (a0 == 251)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24);
            }

            var a5 = ReadByte();
            if (a0 == 252)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32);
            }

            var a6 = ReadByte();
            if (a0 == 253)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40);
            }

            var a7 = ReadByte();
            if (a0 == 254)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48);
            }

            var a8 = ReadByte();
            if (a0 == 255)
            {
                return a1 + (((ulong)a2) << 8) + (((ulong)a3) << 16) + (((ulong)a4) << 24) + (((ulong)a5) << 32) + (((ulong)a6) << 40) + (((ulong)a7) << 48) + (((ulong)a8) << 56);
            }

            throw new IndexOutOfRangeException("ReadULongCompressed() failure: " + a0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float ReadFloat()
        {
            return FloatConversion.ToSingle(ReadUInt());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float ReadFloatCompressed()
        {
            const float floatPrecision = 100f;
            return ReadIntCompressed() / floatPrecision;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double ReadDouble()
        {
            return FloatConversion.ToDouble(ReadULong());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double ReadDoubleCompressed()
        {
            const double doublePrecision = 1000.0;
            return ReadLongCompressed() / doublePrecision;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string ReadString()
        {
            var numBytes = ReadUShort();

            if (numBytes == 0)
            {
                return "";
            }

            if (numBytes >= MaxStringLength)
            {
                throw new IndexOutOfRangeException("ReadString() too long: " + numBytes);
            }

            while (numBytes > stringReaderBuffer.Length)
            {
                stringReaderBuffer = new byte[stringReaderBuffer.Length * 2];
            }

            buffer.ReadBytes(stringReaderBuffer, numBytes);

            return new string(encoding.GetChars(stringReaderBuffer, 0, numBytes));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public char ReadChar()
        {
            return (char)buffer.ReadByte();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ReadBoolean()
        {
            int value = buffer.ReadByte();
            return value == 1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] ReadBytes(int count)
        {
            if (count < 0)
            {
                throw new IndexOutOfRangeException("Reader ReadBytes " + count);
            }

            var value = new byte[count];
            buffer.ReadBytes(value, (uint)count);
            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] ReadBytesAndSize()
        {
            var sz = ReadUShort();
            return sz == 0 ? null : ReadBytes(sz);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector2 ReadVector2()
        {
            return new Vector2(ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector2 ReadVector2Compressed()
        {
            return new Vector2(ReadFloatCompressed(), ReadFloatCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 ReadVector3()
        {
            return new Vector3(ReadFloat(), ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 ReadVector3Compressed()
        {
            return new Vector3(ReadFloatCompressed(), ReadFloatCompressed(), ReadFloatCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector4 ReadVector4()
        {
            return new Vector4(ReadFloat(), ReadFloat(), ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector4 ReadVector4Compressed()
        {
            return new Vector4(ReadFloatCompressed(), ReadFloatCompressed(), ReadFloatCompressed(), ReadFloatCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Color ReadColor()
        {
            return new Color(ReadFloat(), ReadFloat(), ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Color32 ReadColor32()
        {
            return new Color32(ReadByte(), ReadByte(), ReadByte(), ReadByte());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Quaternion ReadQuaternion()
        {
            return new Quaternion(ReadFloat(), ReadFloat(), ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Quaternion ReadQuaternionCompressed()
        {
            const float floatPrecision = 10000f;
            var maxIndex = ReadByte();

            if (maxIndex >= 4)
            {
                var x = maxIndex == 4 ? 1f : 0f;
                var y = maxIndex == 5 ? 1f : 0f;
                var z = maxIndex == 6 ? 1f : 0f;
                var w = maxIndex == 7 ? 1f : 0f;
                return new Quaternion(x, y, z, w);
            }

            var a = ReadShort() / floatPrecision;
            var b = ReadShort() / floatPrecision;
            var c = ReadShort() / floatPrecision;
            var d = (float)Math.Sqrt(1f - (a * a + b * b + c * c));

            switch (maxIndex)
            {
                case 0: return new Quaternion(d, a, b, c);
                case 1: return new Quaternion(a, d, b, c);
                case 2: return new Quaternion(a, b, d, c);
                default: return new Quaternion(a, b, c, d);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Rect ReadRect()
        {
            return new Rect(ReadFloat(), ReadFloat(), ReadFloat(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Plane ReadPlane()
        {
            return new Plane(ReadVector3(), ReadFloat());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Ray ReadRay()
        {
            return new Ray(ReadVector3(), ReadVector3());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Matrix4x4 ReadMatrix4x4()
        {
            return new Matrix4x4
            {
                m00 = ReadFloat(),
                m01 = ReadFloat(),
                m02 = ReadFloat(),
                m03 = ReadFloat(),
                m10 = ReadFloat(),
                m11 = ReadFloat(),
                m12 = ReadFloat(),
                m13 = ReadFloat(),
                m20 = ReadFloat(),
                m21 = ReadFloat(),
                m22 = ReadFloat(),
                m23 = ReadFloat(),
                m30 = ReadFloat(),
                m31 = ReadFloat(),
                m32 = ReadFloat(),
                m33 = ReadFloat()
            };
        }

        public NetworkId ReadNetworkId()
        {
            return new NetworkId(ReadUIntCompressed());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T ReadMessage<T>() where T : INetMessage, new()
        {
            var message = MessageCache<T>.Pop();

            try
            {
                message.OnDeserialize(this);
            }
            finally
            {
                MessageCache<T>.Push(message);
            }

            return message;
        }

        public override string ToString()
        {
            return buffer.ToString();
        }
    }
}