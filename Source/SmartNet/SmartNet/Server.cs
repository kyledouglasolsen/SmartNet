﻿using Debug = UnityEngine.Debug;
using NetworkError = UnityEngine.Networking.NetworkError;
using NetworkEventType = UnityEngine.Networking.NetworkEventType;
using NetworkID = UnityEngine.Networking.Types.NetworkID;
using NodeID = UnityEngine.Networking.Types.NodeID;
using NetworkTransport = UnityEngine.Networking.NetworkTransport;
using System;
using System.Collections.Generic;
using SmartNet.Messages;

namespace SmartNet
{
    public class Server
    {
        internal static readonly List<Server> Servers = new List<Server>();

        private readonly ConnectionList connections = new ConnectionList();
        private readonly MessageHandlers handlers = new MessageHandlers();
        private readonly byte[] buffer = new byte[ushort.MaxValue];
        private ServerSettings serverSettings;

        public int Port { get; private set; }
        public int HostId { get; private set; }
        public KnownIdentities KnownIdentities { get; } = new KnownIdentities();

        public event Action StartEvent = delegate { }, StopEvent = delegate { };
        public event Action<Connection> NewConnectionEvent = delegate { }, ConnectionDisconnectEvent = delegate { };

        public Server(ServerSettings serverSettings = null)
        {
            NetworkTransport.Init();
            Updater.Init();
            Configure(serverSettings ?? Default.ServerSettings);
        }

        public void Configure(ServerSettings settings)
        {
            serverSettings = settings;
        }

        public void Start(string ip, int port)
        {
            RegisterDefaultHandlers();

            Port = port;
            HostId = NetworkTransport.AddHost(serverSettings.ToHostTopology(), Port, ip);
            Servers.Add(this);
            StartEvent();
        }

        public void Stop()
        {
            NetworkTransport.RemoveHost(HostId);
            Servers.Remove(this);
            StopEvent();
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            RegisterHandler(MessageTypes.GetTypeId(typeof(T)), info =>
            {
                var generic = GenericMessageInfo<T>.Pop(info.Type, info.Connection, info.ReadMessage<T>(), info.IsServer);
                handler(generic);
                GenericMessageInfo<T>.Push(generic);
            });
        }

        public void RegisterHandler(uint messageType, NetworkMessageDelegate handler)
        {
            handlers.Register(messageType, handler);
        }

        public void UnregisterHandler<T>() where T : INetMessage
        {
            var messageType = MessageTypes.GetTypeId(typeof(T));
            UnregisterHandler(messageType);
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public bool HasHandler<T>() where T : INetMessage
        {
            return handlers.HasHandler<T>();
        }

        public bool HasHandler(uint messageType)
        {
            return handlers.HasHandler(messageType);
        }

        protected virtual void RegisterDefaultHandlers()
        {
            RegisterHandler<AddPlayer>(AddPlayer.HandleMessage);
            RegisterHandler(MessageTypes.GetTypeId<IdentityMessage>(), IdentityMessage.HandleMessage);
        }

        public bool SendToAll<T>(T message, int channelId) where T : INetMessage
        {
            return SendToAll(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool SendToAll(INetMessage message, int channelId)
        {
            return SendToAll(MessageTypes.GetTypeId(message.GetType()), message, channelId);
        }

        public bool SendToAll(uint messageType, INetMessage msg, int channelId)
        {
            var result = true;

            for (var i = 0; i < connections.Count; i++)
            {
                var connection = connections[i];

                if (connection != null)
                {
                    result &= connection.Send(messageType, msg, channelId);
                }
            }

            return result;
        }

        public bool SendToObservers<T>(Connection connection, T message, int channelId) where T : INetMessage
        {
            return SendToObservers(connection, MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool SendToObservers(Connection connection, INetMessage message, int channelId)
        {
            return SendToObservers(connection, MessageTypes.GetTypeId(message.GetType()), message, channelId);
        }

        public bool SendToObservers(Connection connection, uint messageType, INetMessage msg, int channelId)
        {
            var result = true;

            foreach (var observer in connection.Observers)
            {
                if (observer != null)
                {
                    result &= observer.Send(messageType, msg, channelId);
                }
            }

            return result;
        }

        internal static void UpdateAll()
        {
            for (var i = 0; i < Servers.Count; ++i)
            {
                Servers[i].Update();
            }
        }

        internal void UpdateConnections()
        {
            for (int i = 0; i < connections.Count; i++)
            {
                connections[i]?.FlushChannels();
            }
        }

        internal void Update()
        {
            if (HostId == -1)
            {
                return;
            }

            NetworkEventType eventType;

            do
            {
                eventType = NetworkTransport.ReceiveFromHost(HostId, out int connectionId, out int channelId, buffer, buffer.Length, out int receivedSize, out byte errorByte);

                switch (eventType)
                {
                    case NetworkEventType.ConnectEvent:
                    {
                        HandleConnect(connectionId, (NetworkError)errorByte);
                        break;
                    }
                    case NetworkEventType.DataEvent:
                    {
                        HandleData(connectionId, channelId, receivedSize, (NetworkError)errorByte);
                        break;
                    }
                    case NetworkEventType.DisconnectEvent:
                    {
                        HandleDisconnect(connectionId);
                        break;
                    }
                    case NetworkEventType.Nothing: break;
                    case NetworkEventType.BroadcastEvent: break;
                    default:
                        Debug.LogError($"Unknown NetworkEventType {eventType}");
                        break;
                }
            } while (eventType != NetworkEventType.Nothing);

            UpdateConnections();
        }

        private void HandleConnect(int connectionId, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogError($"Connection {connectionId} error on connect {error}");
                return;
            }

            NetworkTransport.GetConnectionInfo(HostId, connectionId, out string ip, out int port, out NetworkID netId, out NodeID nodeId, out byte errorByte);
            var newConnection = new Connection(ip, HostId, connectionId, serverSettings, handlers, KnownIdentities, connections, false, true);
            connections.Add(newConnection);
            newConnection.Send(new UpdateTime(), 0);
            NewConnectionEvent(newConnection);
        }

        private void HandleData(int connectionId, int channelId, int receivedSize, NetworkError error)
        {
            var connection = connections.Get(connectionId);

            if (connection == null)
            {
                return;
            }

            if (error != NetworkError.Ok)
            {
                Debug.LogError($"Connection {connectionId} data error {error}");
                return;
            }

            connection.TransportReceive(buffer, receivedSize, channelId);
        }

        private void HandleDisconnect(int connectionId)
        {
            var connection = connections.Get(connectionId);

            if (connection == null)
            {
                return;
            }

            connections.Remove(connectionId);
            connection.Disconnect();
            ConnectionDisconnectEvent(connection);
        }
    }
}