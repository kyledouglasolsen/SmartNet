﻿using System;
using SerializeField = UnityEngine.SerializeField;
using HideInInspector = UnityEngine.HideInInspector;
using MonoBehaviour = UnityEngine.MonoBehaviour;
using Debug = UnityEngine.Debug;
using SmartNet.Messages;
using SmartNet.Profiler;

namespace SmartNet
{
    public class SmartNetIdentity : MonoBehaviour
    {
        private static uint nextNetworkId = 1u;
        private static readonly uint IdentityMessageId = MessageTypes.GetTypeId<IdentityMessage>();

        [SerializeField, HideInInspector] private uint assetId;
        [SerializeField, HideInInspector] private NetworkId networkId;
        [HideInInspector] private Connection owner;
        [HideInInspector] private bool isServer, isOwner;
        [HideInInspector] private KnownIdentities knownIdentities;
        private readonly SetOwner setOwnerMessage = new SetOwner();
        private readonly RemoveOwner removeOwnerMessage = new RemoveOwner();
        private readonly IdentityMessage identityMessage = new IdentityMessage();
        private readonly IdentityHandlers handlers = new IdentityHandlers();
        private INetInitialize[] initializes;
        private INetVisibility visibility;

        public uint AssetId => assetId;
        public NetworkId NetworkId => networkId;
        public Connection Owner => owner;
        public bool IsServer => isServer;
        public bool IsOwner => isOwner;
        public KnownIdentities KnownIdentities => knownIdentities;

        public void SetNetworkId(NetworkId newNetId)
        {
            networkId = newNetId;
        }

        public void SetIsServer(bool newIsServer)
        {
            isServer = newIsServer;
        }

        public void SetKnownIdentities(KnownIdentities newKnownIdentities)
        {
            knownIdentities = newKnownIdentities;
        }

        public void ReplaceOwner(Connection connection)
        {
            if (!IsServer)
            {
                return;
            }

            if (owner == connection)
            {
                return;
            }

            if (owner != null)
            {
                RemoveOwner();
            }

            SetOwner(connection);
        }

        public void SetOwner(Connection connection)
        {
            if (!IsServer)
            {
                return;
            }

            if (owner != null)
            {
                Debug.LogError($"Failed setting owner! Identity is already owned by {Owner}.");
                return;
            }

            SetOwnerInternal(connection);

            setOwnerMessage.NetworkId = NetworkId;
            connection.Send(setOwnerMessage, 1);
        }

        public void RemoveOwner()
        {
            if (!IsServer)
            {
                return;
            }

            if (owner == null)
            {
                Debug.LogError("Failed removing owner! Identity isn't currently owned by anyone.");
                return;
            }

            var connection = owner;
            owner = null;
            isOwner = false;

            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnServerOwnerRemoved(connection);
            }

            removeOwnerMessage.NetworkId = NetworkId;
            connection.Send(removeOwnerMessage, 1);
        }

        public void OnPreStart()
        {
            if (IsServer && owner != null)
            {
                if (visibility == null)
                {
                    visibility = GetComponent<INetVisibility>();
                }

                if (visibility == null)
                {
                    visibility = gameObject.AddComponent<DefaultNetVisibility>();
                }
            }

            initializes = GetComponents<INetInitialize>();

            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnPreStart();
            }
        }

        public void OnStartServer()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStartServer();
            }
        }

        public void OnStopServer()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStopServer();
            }
        }

        public void OnStartOwner()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStartOwner();
            }
        }

        public void OnStopOwner()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStopOwner();
            }
        }

        public void OnStartClient()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStartClient();
            }
        }

        public void OnStopClient()
        {
            for (var i = 0; i < initializes.Length; ++i)
            {
                initializes[i].OnStopClient();
            }
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler) where T : INetMessage, new()
        {
            var messageType = MessageTypes.GetTypeId(typeof(T));

            RegisterHandler(messageType, new T(), info =>
            {
                var generic = GenericMessageInfo<T>.Pop(messageType, Owner, (T)info, IsServer);
                handler(generic);
                GenericMessageInfo<T>.Push(generic);
            });
        }

        public void RegisterHandler<T>(GenericMessageDelegate<T> handler, T message) where T : INetMessage, new()
        {
            var messageType = MessageTypes.GetTypeId(typeof(T));
            var generic = new GenericMessageInfo<T>(messageType, null, message, IsServer);

            RegisterHandler(messageType, message, info =>
            {
                generic.Connection = Owner;
                handler(generic);
            });
        }

        public void UnregisterHandler<T>() where T : INetMessage
        {
            UnregisterHandler(MessageTypes.GetTypeId(typeof(T)));
        }

        public void RegisterHandler(uint messageType, INetMessage cachedMessage, IdentityMessageDelegate handler)
        {
            handlers.Register(messageType, cachedMessage, handler);
        }

        public void UnregisterHandler(uint messageType)
        {
            handlers.Unregister(messageType);
        }

        public void InvokeHandler(uint messageType, INetMessage message)
        {
            var handler = handlers.Get(messageType);

            if (handler == null)
            {
                Debug.LogError($"No handler found on Identity({NetworkId}) for Message {MessageTypes.GetType(messageType)}", this);
                return;
            }

            handler.Handler?.Invoke(message);
        }

        public bool Send<T>(T message, int channelId, bool simulateLocal) where T : INetMessage
        {
            return Send(MessageTypes.GetTypeId<T>(), message, channelId, simulateLocal);
        }

        public bool Send(uint messageType, INetMessage message, int channelId, bool simulateLocal = true)
        {
            if (simulateLocal)
            {
                InvokeHandler(messageType, message);
            }

            identityMessage.Initialize(NetworkId, messageType, message);

            var byteCount = 0u;
            var sent = Owner != null && Owner.Send(IdentityMessageId, identityMessage, channelId, out byteCount);

            if (sent)
            {
                Statistics.Add<IdentityMessage>(NetworkDirection.Outbound, message.GetType().Name, byteCount);
            }

            return sent;
        }

        public bool SendToAll<T>(T message, int channelId) where T : INetMessage
        {
            return SendToAll(MessageTypes.GetTypeId<T>(), message, channelId);
        }

        public bool SendToAll(uint messageType, INetMessage message, int channelId)
        {
            identityMessage.Initialize(NetworkId, messageType, message);
            return NetworkServer.SendToAll(IdentityMessageId, identityMessage, channelId);
        }

        public bool SendToObservers<T>(T message, int channelId) where T : INetMessage
        {
            return SendToObservers(IdentityMessageId, identityMessage, channelId);
        }

        public bool SendToObservers(uint messageType, INetMessage message, int channelId)
        {
            identityMessage.Initialize(NetworkId, messageType, message);
            return NetworkServer.SendToObservers(Owner, IdentityMessageId, identityMessage, channelId);
        }

        public INetMessage GetCachedMessage(uint messageType)
        {
            var message = handlers.Get(messageType)?.Message;

            if (message == null)
            {
                var type = MessageTypes.GetType(messageType);
                message = (INetMessage)Activator.CreateInstance(type);
                handlers.Register(messageType, message, null);
            }

            return message;
        }

        public INetMessage GetCachedMessage<T>() where T : INetMessage, new()
        {
            var message = handlers.Get<T>()?.Message;

            if (message == null)
            {
                handlers.Register(new T(), null);
            }

            return message;
        }

        internal void SetOwnerInternal(Connection connection)
        {
            owner = connection;
            isOwner = connection?.IsMe ?? false;

            if (IsServer)
            {
                for (var i = 0; i < initializes.Length; ++i)
                {
                    initializes[i].OnServerOwnerAdded(owner);
                }
            }
        }

        internal static NetworkId GetNextNetworkId()
        {
            return new NetworkId(nextNetworkId++);
        }

        private void Reset()
        {
            assetId = IdentityLibrary.GetNextPrefabId();
        }
    }
}