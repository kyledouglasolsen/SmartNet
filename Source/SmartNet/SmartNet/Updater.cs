﻿using MonoBehaviour = UnityEngine.MonoBehaviour;
using GameObject = UnityEngine.GameObject;
using HideFlags = UnityEngine.HideFlags;
using System;
using SmartNet.Profiler;
using UnityEngine;

namespace SmartNet
{
    public class Updater : MonoBehaviour
    {
        private static Updater instance;

        public static event Action PreUpdateEvent = delegate { }, PostUpdateEvent = delegate { };

        public static void Init()
        {
            if (instance != null)
            {
                return;
            }

            instance = new GameObject("SmartNet.Updater").AddComponent<Updater>();
            instance.gameObject.hideFlags = HideFlags.HideAndDontSave;
            DontDestroyOnLoad(instance.gameObject);
        }

        private void Update()
        {
            if (Server.Servers.Count > 0)
            {
                NetworkTime.Update(true);
            }

            NetworkTime.Update(false);

            PreUpdateEvent();

            Server.UpdateAll();
            Client.UpdateAll();

            PostUpdateEvent();

            Statistics.Tick(Time.time);
        }
    }
}