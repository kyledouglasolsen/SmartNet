﻿using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;
using Color = UnityEngine.Color;
using Color32 = UnityEngine.Color32;
using Quaternion = UnityEngine.Quaternion;
using Rect = UnityEngine.Rect;
using Plane = UnityEngine.Plane;
using Ray = UnityEngine.Ray;
using Matrix4x4 = UnityEngine.Matrix4x4;
using Debug = UnityEngine.Debug;
using System;
using System.Runtime.CompilerServices;
using System.Text;
using SmartNet.Messages;

namespace SmartNet
{
    public class Writer
    {
        private const int MaxStringLength = 1024 * 32;
        private readonly Buffer buffer;
        private byte[] stringWriteBuffer;
        private static Encoding encoding;
        private const byte TrueByte = 1, FalseByte = 0;

        public Writer()
        {
            buffer = new Buffer();
            encoding = encoding ?? new UTF8Encoding();
            stringWriteBuffer = new byte[MaxStringLength];
        }

        public Writer(byte[] buffer)
        {
            this.buffer = new Buffer(buffer);
            encoding = encoding ?? new UTF8Encoding();
            stringWriteBuffer = new byte[MaxStringLength];
        }

        public uint Position => buffer.Position;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] ToArray()
        {
            var newArray = new byte[buffer.GetReadBytesAsSegment().Count];
            Array.Copy(buffer.GetReadBytesAsSegment().Array, newArray, buffer.GetReadBytesAsSegment().Count);
            return newArray;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] AsArray()
        {
            return buffer.AsArray();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> AsSegment()
        {
            return buffer.AsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetReadBytesAsSegment()
        {
            return buffer.GetReadBytesAsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> GetRemainingBytesAsSegment()
        {
            return buffer.GetRemainingBytesAsSegment();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(char value)
        {
            buffer.WriteByte((byte)value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte value)
        {
            buffer.WriteByte(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(sbyte value)
        {
            buffer.WriteByte((byte)value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(short value)
        {
            buffer.WriteByte2((byte)(value & 0xff), (byte)((value >> 8) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ushort value)
        {
            buffer.WriteByte2((byte)(value & 0xff), (byte)((value >> 8) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int value)
        {
            buffer.WriteByte4((byte)(value & 0xff), (byte)((value >> 8) & 0xff), (byte)((value >> 16) & 0xff), (byte)((value >> 24) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(int value)
        {
            var encoded = Buffer.ZigZagEncode(value);
            WriteCompressed(encoded);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(uint value)
        {
            buffer.WriteByte4((byte)(value & 0xff), (byte)((value >> 8) & 0xff), (byte)((value >> 16) & 0xff), (byte)((value >> 24) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(uint value)
        {
            if (value <= 240)
            {
                Write((byte)value);
                return;
            }
            if (value <= 2287)
            {
                Write((byte)((value - 240) / 256 + 241));
                Write((byte)((value - 240) % 256));
                return;
            }
            if (value <= 67823)
            {
                Write((byte)249);
                Write((byte)((value - 2288) / 256));
                Write((byte)((value - 2288) % 256));
                return;
            }
            if (value <= 16777215)
            {
                Write((byte)250);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                return;
            }

            Write((byte)251);
            Write((byte)(value & 0xFF));
            Write((byte)((value >> 8) & 0xFF));
            Write((byte)((value >> 16) & 0xFF));
            Write((byte)((value >> 24) & 0xFF));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(long value)
        {
            buffer.WriteByte8((byte)(value & 0xff), (byte)((value >> 8) & 0xff), (byte)((value >> 16) & 0xff), (byte)((value >> 24) & 0xff), (byte)((value >> 32) & 0xff), (byte)((value >> 40) & 0xff),
                (byte)((value >> 48) & 0xff), (byte)((value >> 56) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(long value)
        {
            var encoded = Buffer.ZigZagEncode(value);
            WriteCompressed(encoded);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ulong value)
        {
            buffer.WriteByte8((byte)(value & 0xff), (byte)((value >> 8) & 0xff), (byte)((value >> 16) & 0xff), (byte)((value >> 24) & 0xff), (byte)((value >> 32) & 0xff), (byte)((value >> 40) & 0xff),
                (byte)((value >> 48) & 0xff), (byte)((value >> 56) & 0xff));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(ulong value)
        {
            if (value <= 240)
            {
                Write((byte)value);
                return;
            }

            if (value <= 2287)
            {
                Write((byte)((value - 240) / 256 + 241));
                Write((byte)((value - 240) % 256));
                return;
            }

            if (value <= 67823)
            {
                Write((byte)249);
                Write((byte)((value - 2288) / 256));
                Write((byte)((value - 2288) % 256));
                return;
            }

            if (value <= 16777215)
            {
                Write((byte)250);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                return;
            }

            if (value <= 4294967295)
            {
                Write((byte)251);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                Write((byte)((value >> 24) & 0xFF));
                return;
            }

            if (value <= 1099511627775)
            {
                Write((byte)252);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                Write((byte)((value >> 24) & 0xFF));
                Write((byte)((value >> 32) & 0xFF));
                return;
            }

            if (value <= 281474976710655)
            {
                Write((byte)253);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                Write((byte)((value >> 24) & 0xFF));
                Write((byte)((value >> 32) & 0xFF));
                Write((byte)((value >> 40) & 0xFF));
                return;
            }

            if (value <= 72057594037927935)
            {
                Write((byte)254);
                Write((byte)(value & 0xFF));
                Write((byte)((value >> 8) & 0xFF));
                Write((byte)((value >> 16) & 0xFF));
                Write((byte)((value >> 24) & 0xFF));
                Write((byte)((value >> 32) & 0xFF));
                Write((byte)((value >> 40) & 0xFF));
                Write((byte)((value >> 48) & 0xFF));
                return;
            }

            Write((byte)255);
            Write((byte)(value & 0xFF));
            Write((byte)((value >> 8) & 0xFF));
            Write((byte)((value >> 16) & 0xFF));
            Write((byte)((value >> 24) & 0xFF));
            Write((byte)((value >> 32) & 0xFF));
            Write((byte)((value >> 40) & 0xFF));
            Write((byte)((value >> 48) & 0xFF));
            Write((byte)((value >> 56) & 0xFF));
        }

        private static UIntFloat floatConverter;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(float value)
        {
            floatConverter.floatValue = value;
            Write(floatConverter.uIntValue);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(float value)
        {
            const float floatPrecision = 100f;
            WriteCompressed((int)(value * floatPrecision));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(double value)
        {
            floatConverter.doubleValue = value;
            Write(floatConverter.uLongValue);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(double value)
        {
            const double doublePrecision = 1000.0;
            WriteCompressed((long)(value * doublePrecision));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(string value)
        {
            if (value == null)
            {
                buffer.WriteByte2(0, 0);
                return;
            }

            var len = encoding.GetByteCount(value);

            if (len >= MaxStringLength)
            {
                throw new IndexOutOfRangeException("Serialize(string) too long: " + value.Length);
            }

            Write((ushort)len);
            var numBytes = encoding.GetBytes(value, 0, value.Length, stringWriteBuffer, 0);
            buffer.WriteBytes(stringWriteBuffer, (ushort)numBytes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(bool value)
        {
            buffer.WriteByte(value ? TrueByte : FalseByte);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte[] bytes, uint count)
        {
            if (count > ushort.MaxValue)
            {
                Debug.LogError($"Writer Write: buffer is too large ({count}) bytes. The maximum buffer size is 64K bytes.");
                return;
            }

            buffer.WriteBytes(bytes, count);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Writer writer)
        {
            buffer.WriteBuffer(writer.buffer);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte[] bytes, uint offset, uint count)
        {
            if (count > ushort.MaxValue)
            {
                Debug.LogError($"Writer Write: buffer is too large ({count}) bytes. The maximum buffer size is 64K bytes.");
                return;
            }

            buffer.WriteBytesAtOffset(bytes, offset, count);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBytesAndSize(byte[] bytes, uint count)
        {
            if (bytes == null || count == 0)
            {
                Write((ushort)0);
                return;
            }

            if (count > ushort.MaxValue)
            {
                Debug.LogError($"Writer WriteBytesAndSize: buffer is too large ({count}) bytes. The maximum buffer size is 64K bytes.");
                return;
            }

            Write((ushort)count);
            buffer.WriteBytes(bytes, count);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBytesFull(byte[] bytes)
        {
            if (bytes == null)
            {
                Write((ushort)0);
                return;
            }

            if (bytes.Length > ushort.MaxValue)
            {
                Debug.LogError("Writer WriteBytes: buffer is too large (" + bytes.Length + ") bytes. The maximum buffer size is 64K bytes.");
                return;
            }

            Write((ushort)bytes.Length);
            buffer.WriteBytes(bytes, (ushort)bytes.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Vector2 value)
        {
            Write(value.x);
            Write(value.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(Vector2 value)
        {
            WriteCompressed(value.x);
            WriteCompressed(value.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Vector3 value)
        {
            Write(value.x);
            Write(value.y);
            Write(value.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(Vector3 value)
        {
            WriteCompressed(value.x);
            WriteCompressed(value.y);
            WriteCompressed(value.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Vector4 value)
        {
            Write(value.x);
            Write(value.y);
            Write(value.z);
            Write(value.w);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(Vector4 value)
        {
            WriteCompressed(value.x);
            WriteCompressed(value.y);
            WriteCompressed(value.z);
            WriteCompressed(value.w);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Color value)
        {
            Write(value.r);
            Write(value.g);
            Write(value.b);
            Write(value.a);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Color32 value)
        {
            Write(value.r);
            Write(value.g);
            Write(value.b);
            Write(value.a);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Quaternion value)
        {
            Write(value.x);
            Write(value.y);
            Write(value.z);
            Write(value.w);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteCompressed(Quaternion value)
        {
            const float floatPrecision = 10000f;
            byte maxIndex = 0;
            var maxValue = float.MinValue;
            var sign = 1f;

            for (var i = 0; i < 4; i++)
            {
                var element = value[i];
                var abs = Math.Abs(value[i]);

                if (abs > maxValue)
                {
                    sign = element < 0 ? -1 : 1;
                    maxIndex = (byte)i;
                    maxValue = abs;
                }
            }

            if (Math.Abs(maxValue - 1f) < 0.0001f)
            {
                Write((byte)(maxIndex + 4));
                return;
            }

            short a, b, c;

            switch (maxIndex)
            {
                case 0:
                    a = (short)(value.y * sign * floatPrecision);
                    b = (short)(value.z * sign * floatPrecision);
                    c = (short)(value.w * sign * floatPrecision);
                    break;
                case 1:
                    a = (short)(value.x * sign * floatPrecision);
                    b = (short)(value.z * sign * floatPrecision);
                    c = (short)(value.w * sign * floatPrecision);
                    break;
                case 2:
                    a = (short)(value.x * sign * floatPrecision);
                    b = (short)(value.y * sign * floatPrecision);
                    c = (short)(value.w * sign * floatPrecision);
                    break;
                default:
                    a = (short)(value.x * sign * floatPrecision);
                    b = (short)(value.y * sign * floatPrecision);
                    c = (short)(value.z * sign * floatPrecision);
                    break;
            }

            Write(maxIndex);
            Write(a);
            Write(b);
            Write(c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Rect value)
        {
            Write(value.xMin);
            Write(value.yMin);
            Write(value.width);
            Write(value.height);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Plane value)
        {
            Write(value.normal);
            Write(value.distance);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Ray value)
        {
            Write(value.direction);
            Write(value.origin);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Matrix4x4 value)
        {
            Write(value.m00);
            Write(value.m01);
            Write(value.m02);
            Write(value.m03);
            Write(value.m10);
            Write(value.m11);
            Write(value.m12);
            Write(value.m13);
            Write(value.m20);
            Write(value.m21);
            Write(value.m22);
            Write(value.m23);
            Write(value.m30);
            Write(value.m31);
            Write(value.m32);
            Write(value.m33);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(NetworkId netId)
        {
            WriteCompressed(netId.Value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SeekZero()
        {
            buffer.SeekZero();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write<T>(T message) where T : INetMessage
        {
            message.OnSerialize(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(INetMessage message)
        {
            message.OnSerialize(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void StartMessage<T>() where T : INetMessage
        {
            StartMessage(MessageTypes.GetTypeId(typeof(T)));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void StartMessage(uint messageType)
        {
            SeekZero();
            buffer.WriteByte2(0, 0);
            Write((ushort)messageType);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void FinishMessage()
        {
            buffer.FinishMessage();
        }
    }
}